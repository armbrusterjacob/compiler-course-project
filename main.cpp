#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "lexer.hpp"
#include "parser.hpp"
#include "declaration.hpp"
#include "codegen.hpp"

int main(int argc, char *argv[])
{
	std::string filename;
	if(argc > 1)
		filename = argv[1];

	std::ifstream ifs(filename);
	std::stringstream ss;
	ss << ifs.rdbuf();
	std::string fileOutput = ss.str();

	Parser par(fileOutput);
	Decl* pd = par.parse_program();

	generate(pd);

	ifs.close();
	return 0;
}