def glob : int = 3;

def foo(id1 : int, id2 : bool, id3 : float) -> void
{
	return;
}

def ect() -> int 
{
	if(x >= 3) 
		continue; 
	else 
		return;
	x = 3;
	while(true)
		continue;
	if(x == 3) 
		break; 
	else 
		return;
	var identif : int = 3 + 1;
	let ide : float = 999;
	def id : bool = true;
}

def expressionTest() -> void
{
	9;
	identif;
	(3 + ident);
	id[3];
	+ 10;
}

def typeTest() -> void
{
	#var id : [type] = [expression]
	var id :(int, char, bool) -> int = 9;
	var id : (char) = y;
}