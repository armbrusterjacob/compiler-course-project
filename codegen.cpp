#include "codegen.hpp"
#include "type.hpp"
#include "expression.hpp"
#include "declaration.hpp"
#include "statement.hpp"

#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Type.h>
#include <llvm/IR/Constant.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/Support/raw_ostream.h>

#include <cassert>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <stack>
#include <fstream>

/// Associates declarations with values.
using variable_map = std::unordered_map<const Decl*, llvm::Value*>;

// -------------------------------------------------------------------------- //
// Root context

/// The root code generation context. This provides facilities for 
/// translating Beaker types and constants into LLVM types and constants.
struct cg_context
{
  cg_context()
    : ll(new llvm::LLVMContext())
  { }

  /// Destroys the codegen context.
  ~cg_context() { delete ll; }

  /// Returns the LLVM context.
  llvm::LLVMContext *get_context() const { return ll; }

  // Names

  /// Returns a name for the declaration.
  std::string get_name(const Decl* d);

  // Types

  /// Generate the corresponding type for `t`.
  llvm::Type* get_type(const Type* t);
  llvm::Type* get_bool_type(const BoolType* t);
  llvm::Type* get_char_type(const CharType* t);
  llvm::Type* get_int_type(const IntType* t);
  llvm::Type* get_float_type(const FloatType* t);
  llvm::Type* get_ref_type(const ReferenceType* t);
  llvm::Type* get_fn_type(const CallType* t);

  /// Returns the corresponding type for the declaration `d`.
  llvm::Type* get_type(const TypedDecl* d);

  /// The underlying LLVM context.
  llvm::LLVMContext* ll;
};

// -------------------------------------------------------------------------- //
// Module context

/// The module generation context provides facilities for generating 
/// and referencing module-level declarations.
struct cg_module
{
  cg_module(cg_context& cxt, const ProgramDecl* prog);

  /// Returns the LLVM context.
  llvm::LLVMContext* get_context() const { return parent->get_context(); }

  /// Returns the underlying LLVM module.
  llvm::Module* get_module() const { return mod; }

  //  Names

  /// Generates a declaration name for `d`.
  std::string get_name(const Decl* d) { return parent->get_name(d); }

  // Types

  /// Generate a corresponding type to `t`.
  llvm::Type* get_type(const Type* t) { return parent->get_type(t); }

  /// Generates a type corresponding to the type `d`.
  llvm::Type* get_type(const TypedDecl* d) { return parent->get_type(d); }

  // Global values

  /// Associate the value `v` with the global declaration `d`.
  void declare(const Decl* d, llvm::GlobalValue* v);

  /// Returns the global value corresponding to `d` or nullptr.
  llvm::GlobalValue* lookup(const Decl* d) const;

  // Declaration generation

  /// Process expressions as top-level declarations.
  void generate();
  void generate(const Decl* d);
  void generate_var_decl(const VariableDecl* d);
  void generate_fn_decl(const FunctionDecl* d);

  /// The parent context.
  cg_context* parent; 

  /// The corresponding translation unit.
  const ProgramDecl* prog;  
  
  /// The underlying LLVM module.
  llvm::Module* mod; 

  /// A lookup table for global modules.
  variable_map globals;
};

// -------------------------------------------------------------------------- //
// Function context

/// Provides the codegen context for expressions.
struct cg_function
{
  cg_function(cg_module& m, const FunctionDecl* d);

  // Context

  /// Returns the LLVM context.
  llvm::LLVMContext* get_context() const { return parent->get_context(); }

  /// Returns the owning LLVM module.
  llvm::Module* get_module() const { return parent->get_module(); }

  /// Returns the underlying LLVM Function.
  llvm::Function* get_function() const { return fn; }

  // Names

  /// Returns the name for the declaration `d`.
  std::string get_name(const Decl* d) { return parent->get_name(d); }

  // Types

  /// Generates the type corresponding to the type `t`.
  llvm::Type* get_type(const Type* t) { return parent->get_type(t); }

  /// Generates the type corresponding to the expression `e`.
  llvm::Type* get_type(const Expr* e) { return get_type(e->getType()); }

  /// Generate the corresponding decl for `td`.
  llvm::Type* get_type(const TypedDecl* t) { return parent->get_type(t); }

  // Local variables

  /// Declare a new local value.
  void declare(const Decl* x, llvm::Value* v);

  /// Lookup a value. This may return a global value.
  llvm::Value* lookup(const Decl* x) const;

  // Function definition

  void define();

  // Block management

  /// Returns the entry block. This is where local variables are allocated.
  llvm::BasicBlock* get_entry_block() const { return entry; }

  /// Returns the current block.
  llvm::BasicBlock* get_current_block() const { return curr; }

  /// Returns a new block with the given name. The block is unlinked until
  /// it is emitted.
  llvm::BasicBlock* make_block(const char* label);

  /// Emits a new block, making it active.
  void emit_block(llvm::BasicBlock* bb);

  // Instruction generation

  /// Generate a list of instructions to compute the value of e.
  llvm::Value* generate_expr(const Expr* e);
  llvm::Value* generate_bool_expr(const BoolExpr* e);
  llvm::Value* generate_int_expr(const IntExpr* e);
  llvm::Value* generate_float_expr(const FloatExpr* e);
  llvm::Value* generate_id_expr(const IdExpr* e);
  llvm::Value* generate_unop_expr(const UnaryExpr* e);
  llvm::Value* generate_arithmetic_expr(const UnaryExpr* e);
  llvm::Value* generate_int_expr(const UnaryExpr* e);
  llvm::Value* generate_float_expr(const UnaryExpr* e);
  llvm::Value* generate_bitwise_expr(const UnaryExpr* e);
  llvm::Value* generate_logical_expr(const UnaryExpr* e);
  llvm::Value* generate_address_expr(const UnaryExpr* e);
  llvm::Value* generate_deref_expr(const UnaryExpr* e);
  llvm::Value* generate_binop_expr(const BinaryExpr* e);
  llvm::Value* generate_arithmetic_expr(const BinaryExpr* e);
  llvm::Value* generate_int_expr(const BinaryExpr* e);
  llvm::Value* generate_float_expr(const BinaryExpr* e);
  llvm::Value* generate_bitwise_expr(const BinaryExpr* e);
  llvm::Value* generate_logical_expr(const BinaryExpr* e);
  llvm::Value* generate_and_expr(const BinaryExpr* e);
  llvm::Value* generate_or_expr(const BinaryExpr* e);
  llvm::Value* generate_relational_expr(const BinaryExpr* e);
  llvm::Value* generate_call_expr(const CallExpr* e);
  llvm::Value* generate_index_expr(const IndexExpr* e);
  llvm::Value* generate_cast_expr(const CastExpr* e);
  llvm::Value* generate_cond_expr(const ConditionalExpr* e);
  llvm::Value* generate_assign_expr(const BinaryExpr* e); //switch from its own expr to binary
  llvm::Value* generate_conv_expr(const ConversionExpr* e);

  // Statements
  void generate_stmt(const Stmt* s);
  void generate_block_stmt(const BlockStmt* s);
  // void generate_when_stmt(const when_stmt* s); // no when statement
  void generate_if_stmt(const IfStmt* s);
  void generate_while_stmt(const WhileStmt* s);
  void generate_break_stmt(const BreakStmt* s);
  void generate_cont_stmt(const ContinueStmt* s);
  void generate_ret_stmt(const ReturnStmt* s);
  void generate_decl_stmt(const DeclarationStmt* s);
  void generate_expr_stmt(const ExpressionStmt* s);

  // Local declarations
  void generate_decl(const Decl* d);
  void generate_var_decl(const VariableDecl* d);

  void make_variable(const VariableDecl* d);
  void make_reference(const VariableDecl* d);

  void push_to_while_stack(llvm::BasicBlock* start, llvm::BasicBlock* end);
  void pop_while_stack();
  llvm::BasicBlock* get_while_start();
  llvm::BasicBlock* get_while_end();
  bool is_while_empty();

  /// The parent module context.
  cg_module* parent;

  /// The function original function
  const FunctionDecl* src;
  
  /// The underlying function being defined
  llvm::Function* fn;
  
  /// The entry block.
  llvm::BasicBlock* entry;

  /// The current block.
  llvm::BasicBlock* curr; 

  // Points to the the condition of a loop in a while statement
  // For continue statements
  std::stack<llvm::BasicBlock*> while_start;

  // Points to the end of a a while statement
  // For break statements
  std::stack<llvm::BasicBlock*> while_end;
  
  /// Local variables.
  variable_map locals;
};

// -------------------------------------------------------------------------- //
// Context implementation

std::string
cg_context::get_name(const Decl* d)
{
  assert(d);
  return d->getName();
}

/// Generate the corresponding type.
llvm::Type* 
cg_context::get_type(const Type* t)
{
  // Make sure we're looking at the semantic, not lexical type.
  switch (t->getKind()) {
  case ty_bool:
    return get_bool_type(static_cast<const BoolType*>(t));
  case ty_char:
    return get_char_type(static_cast<const CharType*>(t));
  case ty_int:
    return get_int_type(static_cast<const IntType*>(t));
  case ty_float:
    return get_float_type(static_cast<const FloatType*>(t));
  case ty_pointer:
  case ty_reference:
    return get_ref_type(static_cast<const ReferenceType*>(t));  
  case ty_call:
    return get_fn_type(static_cast<const CallType*>(t));
  default:
    throw std::logic_error("invalid type, ");
  }
}

/// The corresponding type is i1.
llvm::Type*
cg_context::get_bool_type(const BoolType* t)
{
  return llvm::Type::getInt1Ty(*ll);
}

/// The corresponding type is i8.
llvm::Type*
cg_context::get_char_type(const CharType* t)
{
  return llvm::Type::getInt8Ty(*ll);
}

/// The corresponding type is i32.
llvm::Type*
cg_context::get_int_type(const IntType* t)
{
  return llvm::Type::getInt32Ty(*ll);
}

/// The corresponding type is float.
llvm::Type*
cg_context::get_float_type(const FloatType* t)
{
  return llvm::Type::getFloatTy(*ll);
}

/// Returns a pointer to the object type.
llvm::Type*
cg_context::get_ref_type(const ReferenceType* t)
{
  llvm::Type* obj = get_type(t->getObject());
  return obj->getPointerTo();
}

/// Generate the type as a pointer. The actual function type can extracted
/// as needed for creating functions.
llvm::Type* 
cg_context::get_fn_type(const CallType* t)
{
  const TypeList& ps = t->getTypeList();
  std::vector<llvm::Type*> parms(ps.size());
  std::transform(ps.begin(), ps.end(), parms.begin(), [this](const Type* p) {
    return get_type(p);
  });
  llvm::Type* ret = get_type(t->getReturnType());
  llvm::Type* base = llvm::FunctionType::get(ret, parms, false);
  return base->getPointerTo();
}

llvm::Type*
cg_context::get_type(const TypedDecl* d)
{
  return get_type(d->getType());
}

// -------------------------------------------------------------------------- //
// Module implementation

/// \todo Derive the name of the output file from compiler options.
cg_module::cg_module(cg_context& cxt, const ProgramDecl* prog)
  : parent(&cxt), 
    prog(prog), 
    // FIX: a.ll removed form string to create no source_filename
    mod(new llvm::Module("", *get_context()))
{ }

void
cg_module::declare(const Decl* d, llvm::GlobalValue* v)
{
  assert(globals.count(d) == 0);
  globals.emplace(d, v);
}

llvm::GlobalValue*
cg_module::lookup(const Decl* d) const
{
  auto iter = globals.find(d);
  if (iter != globals.end())
    return llvm::cast<llvm::GlobalValue>(iter->second);
  else
    return nullptr;
}

/// Process top-level declarations.
void 
cg_module::generate()
{
  for (const Decl* d : prog->getDeclList())
    generate(d);
}

void
cg_module::generate(const Decl* d)
{
  switch (d->getKind()) {
  case dl_variable:
    return generate_var_decl(static_cast<const VariableDecl*>(d));
  
  case dl_function:
    return generate_fn_decl(static_cast<const FunctionDecl*>(d));

  default: 
    throw std::logic_error("invalid declaration");
  }
}

/// Generate a variable.
///
/// \todo To declare a global variable, we need to determine if it is
/// statically or dynamically initialized. A variable is statically 
/// initialized if it's initializer can be constant folded. Right now,
/// global variables are simply zero-initialized.
///
/// \todo Make a variable initialization context like we do for functions?
/// That might be pretty elegant.
void 
cg_module::generate_var_decl(const VariableDecl* d)
{
  std::string n = get_name(d);
  llvm::Type* t = get_type(d->getType());
  llvm::Constant* c = llvm::Constant::getNullValue(t);
  llvm::GlobalVariable* var = new llvm::GlobalVariable(
      *mod, t, false, llvm::GlobalVariable::ExternalLinkage, c, n);

  // Create the binding.
  declare(d, var);
}

/// Generate a function from the fn expression.
void 
cg_module::generate_fn_decl(const FunctionDecl* d)
{
  cg_function fn(*this, d);
  fn.define();
}

// -------------------------------------------------------------------------- //
// Function implementation

static llvm::FunctionType*
get_fn_type(llvm::Type* t)
{
  assert(llvm::isa<llvm::PointerType>(t));
  return llvm::cast<llvm::FunctionType>(t->getPointerElementType());
}

cg_function::cg_function(cg_module& m, const FunctionDecl* d)
  : parent(&m), src(d), fn(), entry(), curr()
{
  std::string n = get_name(d);
  llvm::Type* t = get_type(d);
  fn = llvm::Function::Create(
      get_fn_type(t), llvm::Function::ExternalLinkage, n, get_module());

  // Create a binding in the module.
  parent->declare(d, fn);
  
  // Build and emit the entry block.
  entry = make_block("entry");
  emit_block(entry);

  llvm::IRBuilder<> ir(get_current_block());

  // Configure function parameters and declare them as locals.
  assert(d->getDeclList().size() == fn->arg_size());
  auto pi = d->getDeclList().begin();

  DeclList dl = d->getDeclList();
  for(auto it = dl.begin(); it != dl.end(); it++)
  {
    if(!*it)
      std::cout << "param doesnt exist here" << std::endl;
  }

  auto ai = fn->arg_begin();
  while (ai != fn->arg_end()) {
    const ParameterDecl* parm = static_cast<const ParameterDecl*>(*pi);
    llvm::Argument& arg = *ai;

    // Configure each parameter.
    arg.setName(get_name(parm));

    // Declare local variable for each parameter and initialize it
    // with wits corresponding value.
    llvm::Value* var = ir.CreateAlloca(arg.getType(), nullptr, arg.getName());
    declare(parm, var);

    // Initialize with the value of the argument.
    ir.CreateStore(&arg, var);
    
    ++ai;
    ++pi;
  }

}

void
cg_function::declare(const Decl* d, llvm::Value* v)
{
  assert(locals.count(d) == 0);
  locals.emplace(d, v);
}

llvm::Value*
cg_function::lookup(const Decl* d) const
{
  auto iter = locals.find(d);
  if (iter != locals.end())
    return iter->second;
  else
    return parent->lookup(d);
}

llvm::BasicBlock*
cg_function::make_block(const char* label)
{
  return llvm::BasicBlock::Create(*get_context(), label);
}

void
cg_function::emit_block(llvm::BasicBlock* bb)
{
  bb->insertInto(get_function());
  curr = bb;
}

/// Creates a return instruction for the expression.
void
cg_function::define()
{
  generate_stmt(src->getBody());
}

llvm::Value*
cg_function::generate_expr(const Expr* e)
{
  switch (e->getKind()) {
  case ex_bool:
    return generate_bool_expr(static_cast<const BoolExpr*>(e));
  case ex_int:
    return generate_int_expr(static_cast<const IntExpr*>(e));
  case ex_float:
    return generate_float_expr(static_cast<const FloatExpr*>(e));
  case ex_identifer:
    return generate_id_expr(static_cast<const IdExpr*>(e));
  case ex_unary:
    return generate_unop_expr(static_cast<const UnaryExpr*>(e));
  case ex_binary:
    return generate_binop_expr(static_cast<const BinaryExpr*>(e));
  case ex_call:
    return generate_call_expr(static_cast<const CallExpr*>(e));
  case ex_index:
    return generate_index_expr(static_cast<const IndexExpr*>(e));
  case ex_conditional:
    return generate_cond_expr(static_cast<const ConditionalExpr*>(e));
  // again no assignment expr on it's own
  //case expr::assign_kind:
    //return generate_assign_expr(static_cast<const BinaryExpr*>(e));
  case ex_conversion:
    return generate_conv_expr(static_cast<const ConversionExpr*>(e));
  default: 
    throw std::runtime_error("invalid expression");
  }
}

llvm::Value*
cg_function::generate_bool_expr(const BoolExpr* e)
{
  return llvm::ConstantInt::get(get_type(e), e->value(), false);
}

llvm::Value*
cg_function::generate_int_expr(const IntExpr* e)
{
  return llvm::ConstantInt::get(get_type(e), e->value(), true);
}

llvm::Value*
cg_function::generate_float_expr(const FloatExpr* e)
{
  return llvm::ConstantFP::get(get_type(e), e->value());
}

llvm::Value*
cg_function::generate_id_expr(const IdExpr* e)
{
  Decl* d = e->getDecl();

  auto a = this->locals.find(d);
  llvm::Value* alloc = a->second;

  return alloc;
}

llvm::Value*
cg_function::generate_unop_expr(const UnaryExpr* e)
{
  switch(e->getUnaryOp())
  {
    case un_reference:
      return generate_address_expr(e);
    case un_dereference:
      return generate_deref_expr(e);
  }

  // Rest of unary ops require a second value

  llvm::IRBuilder<> ir(get_current_block());
  llvm::Value* lv1 = generate_expr(e->getExpr());

  // Generate a value for the cooresponding unary operation
  int value = 0;
  if(e->getUnaryOp() == un_bit_not)
    value = -1;

  Expr* e2;
  switch(e->getExpr()->getTypeKind())
  {
    case ty_int:
      e2 = new IntExpr(value);
      break;
    case ty_bool:
      e2 = new BoolExpr(value);
      break;
    case ty_float:
      e2 = new FloatExpr(value);
      break;
    default:
      throw std::runtime_error("No cooresponding type in unary execution.");
  }

  llvm::Value* lv2 = generate_expr(e2);
  delete e2;

  if(lv1->getType()->isPointerTy())
    lv1 = ir.CreateLoad(lv1);

  switch(e->getUnaryOp())
  {
    case un_add:
      return ir.CreateAdd(lv2, lv1);
    case un_subtract:
      return ir.CreateSub(lv2, lv1);
    case un_bit_not:
      return ir.CreateXor(lv1, lv2); 
    case un_log_not:
      return ir.CreateNot(lv1);
    default:
      throw std::runtime_error("Unary operation not supported.");
  }
}

// Note that &e is equivalent to e. This is because e is already an address.
llvm::Value*
cg_function::generate_address_expr(const UnaryExpr* e)
{
  llvm::IRBuilder<> ir(get_current_block());
  llvm::Value* lv = generate_expr(e->getExpr());
  return lv;
}

// Note that *e is equivalent to e. This is because e is already an address.
llvm::Value*
cg_function::generate_deref_expr(const UnaryExpr* e)
{
  llvm::IRBuilder<> ir(get_current_block());
  llvm::Value* lv = generate_expr(e->getExpr());
  return ir.CreateLoad(lv);
}

llvm::Value*
cg_function::generate_binop_expr(const BinaryExpr* e)
{
  // std::cout << "; binary e = " << e->getLeftExpr()->getKind() << std::endl;

  // First check and handle cases that pertain to other functions
  switch(e->getBinaryOp())
  {
    case bn_assignment:
      return generate_assign_expr(e);
    case bn_equality:
    case bn_inequality:
    case bn_less_than:
    case bn_less_than_equals:
    case bn_greater_than:
    case bn_greater_than_equals:
      return generate_relational_expr(e);
  }

  llvm::IRBuilder<> ir(get_current_block());

  llvm::Value* lv1 = generate_expr(e->getLeftExpr());
  llvm::Value* lv2 = generate_expr(e->getRightExpr());

  // If a type was originally a pointer load it in for use as a literal.
  if(lv1->getType()->isPointerTy())
    lv1 = ir.CreateLoad(lv1);
  if(lv2->getType()->isPointerTy())
    lv2 = ir.CreateLoad(lv2);


  Type* type = e->getType();

  if(type->isBool())
  {
    switch(e->getBinaryOp())
    {
      case bn_log_and:
        return ir.CreateAnd(lv1, lv2);
      case bn_log_or:
        return ir.CreateOr(lv1, lv2);
    }
  }
  else if(type->isFloat())
  {
    switch(e->getBinaryOp())
    {
      case bn_add:
        return ir.CreateFAdd(lv1, lv2);
      case bn_subtract:
        return ir.CreateFSub(lv1, lv2);
      case bn_multiply:
        return ir.CreateFMul(lv1, lv2);
      case bn_divide:
        return ir.CreateFDiv(lv1, lv2);
      case bn_mod:
        return ir.CreateFRem(lv1, lv2);
    }
  }
  else if(type->isInt())
  {
    switch(e->getBinaryOp())
    {
      case bn_multiply:
        return ir.CreateMul(lv1, lv2);
      case bn_divide:
        return ir.CreateSDiv(lv1, lv2);
      case bn_mod:
        return ir.CreateSRem(lv1, lv2);
      case bn_add:
        return ir.CreateAdd(lv1, lv2);
      case bn_subtract:
        return ir.CreateSub(lv1, lv2);
      case bn_bit_shift_left:
        return ir.CreateShl(lv1, lv2);
      case bn_bit_shift_right:
        return ir.CreateLShr(lv1, lv2);
      case bn_bit_and:
        return ir.CreateAnd(lv1, lv2);
      case bn_bit_or:
        return ir.CreateOr(lv1, lv2);
      case bn_bit_xor:
        return ir.CreateXor(lv1, lv2);
    }
  }

  throw std::runtime_error("Invalid binary operation\
    e = " + e->getLeftExpr()->getKind());
    
}

llvm::Value*
cg_function::generate_relational_expr(const BinaryExpr* e)
{
  llvm::IRBuilder<> ir(get_current_block());

  llvm::Value* lv1 = generate_expr(e->getLeftExpr());
  llvm::Value* lv2 = generate_expr(e->getRightExpr());

  // If a type was originally a pointer load it in for use as a literal.
  if(lv1->getType()->isPointerTy())
    lv1 = ir.CreateLoad(lv1);
  if(lv2->getType()->isPointerTy())
    lv2 = ir.CreateLoad(lv2);

  switch(e->getBinaryOp())
  {
    case bn_equality:
        return ir.CreateICmpEQ(lv1, lv2);
    case bn_inequality:
        return ir.CreateICmpNE(lv1, lv2);
    case bn_less_than:
        return ir.CreateICmpSLT(lv1, lv2);
    case bn_less_than_equals:
        return ir.CreateICmpSLE(lv1, lv2);
    case bn_greater_than:
        return ir.CreateICmpSGT(lv1, lv2);
    case bn_greater_than_equals:
        return ir.CreateICmpSGE(lv1, lv2);
  }
  return nullptr;
}

llvm::Value*
cg_function::generate_call_expr(const CallExpr* e)
{
  llvm::IRBuilder<> ir(get_current_block());
  
  IdExpr* id = static_cast<IdExpr*>(e->getBase());
  FunctionDecl* callee = static_cast<FunctionDecl*>(id->getDecl());

  llvm::Value* lv_function = parent->lookup(callee);

  // Generate each arguement
  ExprList list = e->getList();
  std::vector<llvm::Value*> args;
  for(auto it = list.begin(); it != list.end(); it++)
  {
    args.push_back(generate_expr(*it));
  }

  llvm::Value* a = ir.CreateCall(lv_function, args);
  return a;
}

// Unimplemented
llvm::Value*
cg_function::generate_index_expr(const IndexExpr* e)
{
  throw std::runtime_error("Index Expressions are unimplemented");
  return nullptr;
}

llvm::Value*
cg_function::generate_assign_expr(const BinaryExpr* e)
{
  llvm::IRBuilder<> ir(get_current_block());
  
  Expr* temp = e->getRightExpr();
  IdExpr* id = static_cast<IdExpr*>(temp);

  // Find the llvm allocation
  auto a = this->locals.find(id->getDecl());
  llvm::Value* alloc = a->second;

  // Give the allocation a value from expr
  llvm::Value* lv = generate_expr(e->getLeftExpr());

  // Create a load if neccesary
  if(lv->getType()->isPointerTy())
    lv = ir.CreateLoad(lv);

  return ir.CreateStore(lv, alloc);
}

llvm::Value*
cg_function::generate_cond_expr(const ConditionalExpr* e)
{
  llvm::IRBuilder<> ir(get_current_block());
  llvm::Value* lv_cond = generate_expr(e->getCondExpr());
  llvm::Value* lv1 = generate_expr(e->getTrueExpr());
  llvm::Value* lv2 = generate_expr(e->getFalseExpr());

  return ir.CreateSelect(lv_cond, lv1, lv2);
}

llvm::Value*
cg_function::generate_conv_expr(const ConversionExpr* c)
{
  // std::cout << "; conversion c = " << c->getConversion() << ", t = " << c->getTypeKind() << std::endl;

  Expr* base = c->getBase();
  type_kind tk = base->getTypeKind();
  llvm::IRBuilder<> ir(get_current_block());
  llvm::Value* lv = generate_expr(base);

  switch(c->getConversion())
  {
    case co_pointer:
    case co_reference:
      return lv;
    case co_value:
      if(lv->getType()->isPointerTy() || c->getBase()->isIdentifer())
      {
        lv = ir.CreateLoad(lv);
        c = static_cast<ConversionExpr*>(c->getBase());
      }
      return lv;
  }

  // The rest of the conversions require a type

  llvm::Type* type = get_type(c->getType());

  if(lv->getType()->isPointerTy())
    lv = ir.CreateLoad(lv);

  switch(c->getConversion())
  {
    case co_trunaction:
      return ir.CreateFPToSI(lv, type);
    case co_extension:
      return ir.CreateSIToFP(lv, type);
  }

  switch(tk)
  {
      case ty_int:
        return ir.CreateIntCast(lv, type, true);
      case ty_bool:
        return ir.CreateZExtOrBitCast(lv, type);
      case ty_char:
        //char isnt currently implemented 
        return ir.CreateIntCast(lv, type, true);
      case ty_float:
        return ir.CreateFPCast(lv, type);
      case ty_pointer:
        return ir.CreateBitOrPointerCast(lv, type);
      default:
        throw std::runtime_error("Undefined conversion.");
  }
}

void
cg_function::generate_stmt(const Stmt* s)
{
  switch (s->getKind()) {
  case st_block:
    return generate_block_stmt(static_cast<const BlockStmt*>(s));
  // no when type
  //case stmt::when_kind:
    //return generate_when_stmt(static_cast<const when_stmt*>(s));
  case st_if:
    return generate_if_stmt(static_cast<const IfStmt*>(s));
  case st_while:
    return generate_while_stmt(static_cast<const WhileStmt*>(s));
  case st_break:
    return generate_break_stmt(static_cast<const BreakStmt*>(s));
  case st_continue:
    return generate_cont_stmt(static_cast<const ContinueStmt*>(s));
  case st_return:
    return generate_ret_stmt(static_cast<const ReturnStmt*>(s));
  case st_declaration:
    return generate_decl_stmt(static_cast<const DeclarationStmt*>(s));
  case st_expression:
    return generate_expr_stmt(static_cast<const ExpressionStmt*>(s));
  }
}


void
cg_function::generate_block_stmt(const BlockStmt* s)
{
  llvm::IRBuilder<> ir(get_entry_block());
  StmtList stmts = s->getStmtList();

  // First reserve memory for each declaration
  for(auto it = stmts.begin(); it != stmts.end(); it++)
  {
    Stmt* st = *it;
    if(st->isDeclaration())
    {
      DeclarationStmt* ds = dynamic_cast<DeclarationStmt*>(st);
      Decl* d = ds->getDeclaration();
      if(!d->is_variable())
        continue;
      VariableDecl* vd = dynamic_cast<VariableDecl*>(d);
      llvm::Value* alloc = ir.CreateAlloca(get_type(vd), nullptr, get_name(vd));

      this->declare(d, alloc);
    }
  }

  // For each statement in the function
  for(auto it = stmts.begin(); it != stmts.end(); it++)
  {
    Stmt* st = *it;
    generate_stmt(*it);
  }
}

// again no when statement
// void
// cg_function::generate_when_stmt(const when_stmt* s)
// {
// }

void
cg_function::generate_if_stmt(const IfStmt* s)
{
  llvm::Value* cond = generate_expr(s->getCond());
  llvm::BasicBlock* on_true = make_block("on_true");
  llvm::BasicBlock* on_false = make_block("on_false");
  llvm::BasicBlock* after = make_block("after_if");

  llvm::IRBuilder<> ir(get_current_block());

  // if(expr)stmt
  if(s->hasElse())
  {
    ir.CreateCondBr(cond, on_true, on_false);
    
    emit_block(on_true);
    generate_stmt(s->getTrueStmt());
    llvm::IRBuilder<> irTrue(get_current_block());
    irTrue.CreateBr(after);
    
    emit_block(on_false);
    generate_stmt(s->getFalseStmt());
    llvm::IRBuilder<> ir_false(get_current_block());
    ir_false.CreateBr(after);
  }
  //if(expr)stmt else stmt
  else
  {
    ir.CreateCondBr(cond, on_true, after);

    emit_block(on_true);
    generate_stmt(s->getTrueStmt());
    llvm::IRBuilder<> ir_true(get_current_block());
    ir_true.CreateBr(after);
  }

  emit_block(after);
  return;
}

void
cg_function::generate_while_stmt(const WhileStmt* s)
{
  llvm::BasicBlock* cond_check = make_block("cond_check");
  llvm::BasicBlock* on_true = make_block("on_true");
  // Make sure to point to the next while loop in the case of nested
  // loops. Otherwise create a new label.
  llvm::BasicBlock* after;
  if(is_while_empty())
    after = make_block("after_while");
  else
    after = get_while_end();

  // Set BasicBlock's for break and continue statements to work
  this->push_to_while_stack(cond_check, after);

  llvm::IRBuilder<> ir(get_current_block());
  ir.CreateBr(cond_check);

  emit_block(cond_check);
  llvm::IRBuilder<> ir_loop(get_current_block());

  llvm::Value* loop_cond = generate_expr(s->getCond());
  ir_loop.CreateCondBr(loop_cond, on_true, after);

  emit_block(on_true);
  llvm::IRBuilder<> ir_true(get_current_block());
  generate_stmt(s->getLoop());
  ir_true.CreateBr(cond_check);

  // Return stack to original state before this while loop
  this->pop_while_stack();

  // Unless you are the first while loop, don't create a new branch
  if(is_while_empty())
    emit_block(after);

  return;
}

/*
  Continue and Break depend on while loops with it's helper functions
*/

void
cg_function::generate_break_stmt(const BreakStmt* e)
{
  llvm::IRBuilder<> ir(get_current_block());
  ir.CreateBr(get_while_end());
}

void
cg_function::generate_cont_stmt(const ContinueStmt* e)
{
  llvm::IRBuilder<> ir(get_current_block());
  ir.CreateBr(get_while_start());
}

void
cg_function::generate_ret_stmt(const ReturnStmt* rs)
{
  llvm::IRBuilder<> ir(get_current_block()); 

  // If return has a return expression generate it, othwise return void
  if(rs->hasExpr())
  {
    llvm::Value* lv = generate_expr(rs->getExpr());
    if(lv->getType()->isPointerTy())
      lv = ir.CreateLoad(lv);
    ir.CreateRet(lv);
  }
  else
  {
    ir.CreateRetVoid();
  }
  return;
}

/*
  In generate_block_stmt the values are already allocated so here we just need
  to assign the allocation a value if it requires one.
*/

void
cg_function::generate_decl_stmt(const DeclarationStmt* ds)
{
  // We only need to check if a value is given an expression
  // as all declarations are already allocated in the
  // block statement
  llvm::IRBuilder<> ir(get_current_block());
  Decl* d = ds->getDeclaration();

  // check if we have a varaible statement, and if that
  // statement is given an expr
  if(!d->is_variable())
    return;
  VariableDecl* vd = dynamic_cast<VariableDecl*>(d);
  if(!vd->isExpr())
    return;

  // now retreive allocation from locals map
  auto a = this->locals.find(d);
  llvm::Value* alloc = a->second;


  // give the declaration a value
  llvm::Value* lv = generate_expr(vd->getExpr());
  ir.CreateStore(lv, alloc);
}

void
cg_function::generate_expr_stmt(const ExpressionStmt* es)
{
  Expr* e = es->getExpr();
  generate_expr(e);
  return;
}

/* 
  -- Helper Functions for while statments --
*/

void 
cg_function::push_to_while_stack(llvm::BasicBlock* start, llvm::BasicBlock* end)
{
  this->while_start.push(start);
  this->while_end.push(end);
  return;
}

void 
cg_function::pop_while_stack()
{
  while_start.pop();
  while_end.pop();
}

llvm::BasicBlock* 
cg_function::get_while_start()
{
  return while_start.top();
}

llvm::BasicBlock* 
cg_function::get_while_end()
{
  return while_end.top();
}

bool
cg_function::is_while_empty()
{
  assert(while_start.size() == while_end.size());
  return while_start.empty();
}

/*
  -- Generate called from main --
*/

void
generate(const Decl* d)
{
  assert(d->is_program());
  // Establish the translation context.
  cg_context cg;

  // Create the module, and generate its declarations.
  cg_module mod(cg, static_cast<const ProgramDecl*>(d));
  mod.generate();

  // Dump the generated module to 
  llvm::outs() << *mod.get_module();
}


