#include <unordered_map>
#include "scope.hpp"

void Scope::addToScope(symbol s, Decl* d)
{
	decl_map.insert({s, d});
}

Decl* Scope::lookup(symbol s)
{
	auto declIt = decl_map.find(s);
	if(declIt != decl_map.end())
		return declIt->second;
	return nullptr;
}

Decl* Scope::lookupRecursive(symbol s)
{
	Scope* curr = this;
	while(curr->getParent())
	{
		auto declIt = curr->decl_map.find(s);
		if(declIt != curr->getDeclMap().end())
			return declIt->second;
		curr = curr->getParent();
	}
	return curr->lookup(s);
}