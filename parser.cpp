#include "parser.hpp"
#include <stdexcept>
#include <iostream>

Parser::Parser(const std::string& file)
{
	sem = new Semantics();
	lexer = new Lexer(file);
	fetch();
}

/*
	Return the name of the next token
*/
tokenName Parser::lookahead()
{
	if(!tokenQ.empty())
		return tokenQ.front().getName();
	else
		throw std::runtime_error("Loohahead error");
}

std::string Parser::lookaheadName()
{
	if(!tokenQ.empty())
		return tokenQ.front().toString();
	else
		throw std::runtime_error("lookaheadName error");
}

std::string Parser::getErrorString(std::string text)
{
	if(!tokenQ.empty())
	{
		std::string str = tokenQ.front().getLoc().toString();
		str += " " + text + "\n";
		return str;
	}
	else
		throw std::runtime_error("getErrorString error");
}
/*
	Return the name of the token 'num' spaces ahead
*/
tokenName Parser::lookahead(const int& num)
{
	if(num < tokenQ.size())
		return tokenQ[num].getName();
	for(int i = tokenQ.size(); i < num; i++)
		fetch();
	return tokenQ.back().getName();
}

/*
	Either match, or throw error
*/
Token Parser::match(tokenName tok)
{
	if(lookahead() == tok)
	{
		return accept();
	}
	else
	{
		throw std::runtime_error(getErrorString(
			"match fail [lookahead = " + peak().toString() + 
			", match = " + peak().toString(tok) + "]"));
	}
}

/*
	Consume token, and lex the next
*/
Token Parser::accept()
{
	Token temp = peak();
	tokenQ.pop_front();
	if(tokenQ.empty())
		fetch();
	return temp;
}

/*
	Return the current token without consuming it
*/
Token Parser::peak()
{
	if(!tokenQ.empty())
		return tokenQ.front();
	else
		throw std::runtime_error(getErrorString("peak fail"));
}

bool Parser::peak_if(tokenName tok)
{
	if(peak().getName() == tok)
		return true;
	return false;
}
          
bool Parser::match_if(tokenName tok)
{
	if(tok == lookahead())
	{
		accept();
		return true;
	}
	return false;

}

/*
	Lexes the next token and adds it to the queue
*/
void Parser::fetch()
{
	tokenQ.push_back(lexer->lex());
	return;
}

// -- 2.2 Expressions --

/*
	primary-expression → literal
		| identifier
		| ( expression )
*/
Expr* Parser::parse_primary_expression()
{
	switch(lookahead())
	{
		case tok_bin_int:
		case tok_dec_int:
		case tok_hex_int:
			return sem->on_int_literal(accept());
		case tok_float:
			return sem->on_float_literal(accept());
		case tok_bool:
			return sem->on_bool_literal(accept());
		case tok_char:
			return sem->on_char_literal(accept());
		case tok_string:
		//not implemented because of arrays
			throw std::runtime_error(getErrorString("Strings aren't implemented yet."));
		case tok_identifier:
			return sem->on_identifier(accept());
		case tok_left_paren:
			match(tok_left_paren);
			Expr* e = parse_expression();
			match(tok_right_paren);
			return e;
	}
	std::cout << "last token was: " << peak().toString() << std::endl;
	throw std::runtime_error(getErrorString("Primary Expression Error"));
}

bool Parser::peak_if_primary()
{

	switch(peak().getName())
	{
		case tok_identifier:
		case tok_left_paren:
		case tok_bin_int:
		case tok_dec_int:
		case tok_hex_int:
		case tok_float:
		case tok_bool:
		case tok_char:
			return true;
	}
	return false;

}

/*
	n→ postfix-expression ( argument-list? )
		| postfix-expression [ argument-list? ]
		| primary-expression	
*/
Expr* Parser::parse_postfix_expression()
{
	Expr* e = parse_primary_expression();
	ExprList arg_list;
	switch(lookahead())
	{
		case tok_left_paren:
			match(tok_left_paren);
			
			if(!peak_if(tok_right_paren))
			{
				arg_list = parse_argument_list();
				e = sem->on_function_call(e, arg_list);
				match(tok_right_paren);
			}
			else
			{
				match(tok_right_paren);
			}
			break;
		case tok_left_bracket:
			match(tok_left_bracket);
			if(!peak_if(tok_right_bracket))
				arg_list = parse_argument_list();
				e = sem->on_index(e, arg_list);
			match(tok_right_bracket);
			break;
	}
	return e;
}

bool Parser::match_if_postfix_expression()
{
	switch(lookahead())
	{
		case tok_left_paren:
		case tok_left_bracket:
			accept();
			return true;
	}
	return false;
}
/*
	argument-list → argument-list , argument
		| argument
*/
ExprList Parser::parse_argument_list()
{
	ExprList el = ExprList();
	el.push_back(parse_argument());
	while(match_if_argument_list())
		el.push_back(parse_argument());
	return el;
}

bool Parser::match_if_argument_list()
{
	return match_if(tok_comma);
}

/*
	argument → expression
*/
Expr* Parser::parse_argument()
{
	return parse_expression();
}

/*
	unary-expression → + unary-expression
		| - unary-expression
		| ~ unary-expression
		| ! unary-expression
		| & unary-expression
		| * unary-expression
		| postfix-expression
*/
Expr* Parser::parse_unary_expression()
{
	Expr* e;
	if(peak_if_unary())
	{
		Token tok = accept();
		e = parse_unary_expression();
		e = sem->on_unary(tok, e);
	}
	else
	{
		e = parse_postfix_expression();
	}
	return e;
}

bool Parser::peak_if_unary()
{
	switch(peak().getName())
	{
		case tok_plus:
		case tok_minus:
		case tok_bit_not:
		case tok_log_not:
		case tok_key_not:
		case tok_bit_and:
		case tok_multiply:
			return true;
	}
	return false;
}

/*
	cast-expression → cast-expression as type
		| unary-expression
*/
Expr* Parser::parse_cast_expression()
{
	Expr* e = parse_unary_expression();
	while(match_if_cast())
	{
		Type* t = parse_type();
		e = sem->on_cast_expression(e, t);
	}
	return e;

}

bool Parser::match_if_cast()
{
	return match_if(tok_key_as);
}

/*
	multiplicative-expression → multiplicative-expression * cast-expression
		| multiplicative-expression / cast-expression
		| multiplicative-expression % cast-expression
		| cast-expression	
*/
Expr* Parser::parse_multiplicative_expression()
{
	Expr* e1 = parse_cast_expression();
	while(peak_if_multiplacative())
	{
		Token tok = accept();
		Expr* e2 = parse_cast_expression();
		e1 = sem->on_multiplicative_expression(e1, e2, tok);
	}
	return e1;
}

bool Parser::peak_if_multiplacative()
{
	switch(lookahead())
	{
		case tok_multiply:
		case tok_divide:
		case tok_mod:
			return true;
	}
	return false;
}
/*
	additive-expression → additive-expression + multiplicative-expression
		| additive-expression - multiplicative-expression
		| multiplicative-expression
*/
Expr* Parser::parse_additive_expression()
{
	Expr* e1 = parse_multiplicative_expression();
	while(peak_if_additive())
	{
		Token tok = accept();
		Expr* e2 = parse_multiplicative_expression();
		e1 = sem->on_additive_expression(e1, e2, tok);
	}
	return e1;
}

bool Parser::peak_if_additive()
{
	switch(lookahead())
	{
		case tok_plus:
		case tok_minus:
			return true;
	}
	return false;
}

/*
	shift-expression → shift-expression << additive-expression
		| shift-expression >> additive-expression
		| additive-expression
*/
Expr* Parser::parse_shift_expresion()
{
	Expr* e1 = parse_additive_expression();
	while(peak_if_shift())
	{
		Token tok = accept();
		Expr* e2 = parse_additive_expression();
		e1 = sem->on_shift_expression(e1, e2, tok);
	}
	return e1;
}


bool Parser::peak_if_shift()
{
	switch(lookahead())
	{
		case tok_shift_right:
		case tok_shift_left:
			return true;
	}
	return false;
}

/*
	relational-expression → relational-expression < shift-expression
		| relational-expression > shift-expression
		| relational-expression <= shift-expression
		| relational-expression >= shift-expression
		| shift-expression
*/
Expr* Parser::parse_reltional_expression()
{
	Expr* e1 = parse_shift_expresion();
	while(peak_if_relational())
	{
		Token tok = accept();
		Expr* e2 = parse_shift_expresion();
		e1 = sem->on_relational_expression(e1, e2, tok);
	}
	return e1;
}

bool Parser::peak_if_relational()
{
	switch(lookahead())
	{
		case tok_less_than:
		case tok_less_than_equals:
		case tok_greater_than:
		case tok_greater_than_equals:
			return true;
	}
	return false;
}

/*
	equality-expression → equality-expression == relational-expression
		| equality-expression != relational-expression
		| relational-expression
*/
Expr* Parser::parse_equality_expression()
{
	Expr* e1 = parse_reltional_expression();
	
	while(peak_if_equality())
	{
		Token tok = accept();
		Expr* e2 = parse_reltional_expression();
		e1 = sem->on_equality_expression(e1, e2, tok);
	}
	return e1;
}

bool Parser::peak_if_equality()
{
	switch(lookahead())
	{
		case tok_equals:
		case tok_not_equals:
			return true;
	}
	return false;
}

/*
	bitwise-and-expression → bitwise-and-expression & equality-exprsieson
		| equality-exprsieson
	THIS WAS CHANGED FROM PROJECT TO MATCH TO EQUALITY INSTEAD OF RELATIONAL
*/
Expr* Parser::parse_bitwise_and_expression()
{
	Expr* e1 = parse_equality_expression();
	while(peak_if_bitwise_and())
	{
		Token tok = accept();
		Expr* e2 = parse_equality_expression();
		e1 = sem->on_bitwise_expression(e1, e2, tok);
	}
	return e1;
}

bool Parser::peak_if_bitwise_and()
{
	return peak_if(tok_bit_and);
}
/*
	bitwise-xor-expression → bitwise-xor-expression ^ bitwise-and-expression
		| bitwise-and-expression
*/
Expr* Parser::parse_bitwise_xor_expression()
{
	Expr* e1 = parse_bitwise_and_expression();
	while(peak_if_bitwise_xor())
	{
		Token tok = accept();
		Expr* e2 = parse_bitwise_and_expression();
		e1 = sem->on_bitwise_expression(e1, e2, tok);
	}
	return e1;
}

bool Parser::peak_if_bitwise_xor()
{
	return peak_if(tok_bit_xor);
}
/*
	bitwise-or-expression → bitwise-or-expression | bitwise-xor-expression
		| bitwise-xor-expression
*/
Expr* Parser::parse_bitwise_or_expression()
{
	Expr* e1 = parse_bitwise_xor_expression();
	while(peak_if_bitwise_or())
	{
		Token tok = accept();
		Expr* e2 = parse_bitwise_xor_expression();
		e1 = sem->on_bitwise_expression(e1, e2, tok);
	}
	return e1;
}

bool Parser::peak_if_bitwise_or()
{
	return peak_if(tok_bit_or);
}
/*
	logicial-and-expression → logicial-and-expression and bitwise-or-expression
		| bitwise-or-expression	
*/
Expr* Parser::parse_logical_and_expression()
{
	Expr* e1 = parse_bitwise_or_expression();
	while(peak_if_logical_and())
	{
		Token tok = accept();
		Expr* e2 = parse_bitwise_or_expression();
		e1 = sem->on_logical_expression(e1, e2, tok);
	}
	return e1;
}

bool Parser::peak_if_logical_and()
{
	return peak_if(tok_key_and);
}
/*
	logical-or-expression → logical-or-expression or logical-and-expression
		| logical-and-expression
*/
Expr* Parser::parse_logical_or_expression()
{
	Expr* e1 = parse_logical_and_expression();
	while(peak_if_logical_or())
	{
		Token tok = accept();
		Expr* e2 = parse_logical_and_expression();
		e1 = sem->on_logical_expression(e1, e2, tok);
	}
	return e1;
}

bool Parser::peak_if_logical_or()
{
	return peak_if(tok_key_or);
}

/*
	conditional-expression → logical-or-expression ? expression : conditional-expression
		| logical-or-expression
*/
Expr* Parser::parse_conditional_expression()
{
	Expr* e1 = parse_logical_or_expression();
	while(match_if_conditional())
	{
		Expr* e2 = parse_expression();
		match(tok_colon);
		Expr* e3 = parse_conditional_expression();
		e1 = sem->on_conditional_expression(e1, e2, e3);
	}
	return e1;
}

bool Parser::match_if_conditional()
{
	return match_if(tok_conditional);
}

/*
	assignment-expression → conditional-expression = assignment-expression
		| conditional-expression
*/
Expr* Parser::parse_assignment_expression()
{
	//not sure about this one since its right side
	Expr* e1 = parse_conditional_expression();
	while(match_if_assigment())
	{
		Expr* e2 = parse_assignment_expression();
		e1 = sem->on_assignment_expression(e1, e2);
	}
	return e1;
}

bool Parser::match_if_assigment()
{
	return match_if(tok_assignment);
}

/*
	expression → assignment-expression
*/
Expr* Parser::parse_expression()
{
	parse_assignment_expression();
}

/*
	constant-expression → conditional-expression
*/
Expr* Parser::parse_constant_expression()
{
	parse_conditional_expression();
}


// -- 2.3 Statments --

/*
	statement → block-statement
		| if-statement
		| while-statement
		| break-statement
		| continue-statement
		| break-statement
		| return-statement
		| declaration-statement
		| expression-statement
*/
Stmt* Parser::parse_statement()
{
	switch(lookahead())
	{
		case tok_left_brace:
			return parse_block_statement();
		case tok_key_if:
			return parse_if_statment();
		case tok_key_while:
			return parse_while_statement();
		case tok_key_break:
			return parse_break_statement();
		case tok_key_continue:
			return parse_continue_statement();
		case tok_key_return:
			return parse_return_statement();
	}
	if(peak_if_object_definition())
		return parse_declaration_statment();
	if(peak_if_expression_statement())
		return parse_expression_statement();
	throw std::runtime_error(getErrorString("Undefined statement."));
}

bool Parser::peak_if_statement()
{
	switch(peak().getName())
	{
		case tok_left_brace:
		case tok_key_if:
		case tok_key_while:
		case tok_key_break:
		case tok_key_continue:
		case tok_key_return:
			return true;
	}
	return peak_if_object_definition() || peak_if_expression_statement();
}

/*
	block-statement → { statement-seq? }
*/
Stmt* Parser::parse_block_statement()
{
	sem->enterBlockScope();
	StmtList sl;
	match(tok_left_brace);
	if(!peak_if(tok_right_brace))
		sl = parse_statement_sequence();
	sem->exitScope();
	match(tok_right_brace);
	return sem->on_block_statement(sl);
}

/*
	statement-seq → statement-seq statement
		| statement
*/
StmtList Parser::parse_statement_sequence()
{
	StmtList sl = StmtList();
	sl.push_back(parse_statement());
	while(peak_if_statement())
		sl.push_back(parse_statement());
	return sl;
}

/*
	if-statement → if ( expression ) statement
		| if ( expression ) statement else statement
*/
Stmt* Parser::parse_if_statment()
{
	match(tok_key_if);
	match(tok_left_paren);
	Expr* e = parse_expression();
	match(tok_right_paren);
	Stmt* s1 = parse_statement();
	if(peak_if(tok_key_else))
	{
		match(tok_key_else);
		Stmt* s2 = parse_statement();
		return sem->on_if_statement(e, s1, s2);
	}
	return sem->on_if_statement(e, s1);
}

/*
	while-statement → while ( expression ) statement
*/
Stmt* Parser::parse_while_statement()
{
	match(tok_key_while);
	match(tok_left_paren);
	Expr* e = parse_expression();
	match(tok_right_paren);
	Stmt* s = parse_statement();
	return sem->on_while_statement(e, s);
}

/*
	break-statement → break ;
*/
Stmt* Parser::parse_break_statement()
{
	match(tok_key_break);
	match(tok_semicolon);
	return sem->on_break_statement();
}

/*
	continue-statement → continue ;
*/
Stmt* Parser::parse_continue_statement()
{
	match(tok_key_continue);
	match(tok_semicolon);
	return sem->on_continue_statement();
}

/*
	return-statement → return expression ;
		| return ;
*/
Stmt* Parser::parse_return_statement()
{
	match(tok_key_return);
	
	if(!peak_if(tok_semicolon))
	{
		Expr* e = parse_expression();
		match(tok_semicolon);
		return sem->on_return_statement(e);
	}
	match(tok_semicolon);
	return sem->on_return_statement();

}

/*
	declaration-statement → local-declaration
*/
Stmt* Parser::parse_declaration_statment()
{
	Decl* d = parse_local_declaration();
	return sem->on_declaration_statement(d);
}

/*
	expression-statement → expression ;
*/
Stmt* Parser::parse_expression_statement()
{
	Expr* e = parse_expression();
	match(tok_semicolon);
	return sem->on_expression_statement(e);

}

bool Parser::peak_if_expression_statement()
{
	return peak_if_unary() || peak_if_primary();
}



// -- 2.4 Declarations --

/*
	program → declaration-seq?
*/
Decl* Parser::parse_program()
{
	sem->enterGlobalScope();
	DeclList dl = parse_declaration_sequence();
	return sem->on_program_declaration(dl);
}

/*
	
	declaration-seq → declaration-seq declaration
		| declaration
*/

DeclList Parser::parse_declaration_sequence()
{
	DeclList dl;
	dl.push_back(parse_declaration());
	while(peak_if_declaration())
		dl.push_back(parse_declaration());
	return dl;
}
/*
	declaration → function-definition
		| object-definition
*/
Decl* Parser::parse_declaration()
{
	switch(lookahead())
	{
		case tok_key_let:
		case tok_key_var:
			return parse_object_definition();
		case tok_key_def:
			if(lookahead(3) == tok_colon)
				return parse_object_definition();
			else
				return parse_function_definition();
	}

	throw std::runtime_error(getErrorString("Cannot parse declaration."));
}

bool Parser::peak_if_declaration()
{
	return peak_if_object_definition() || peak_if_function_definition();
}
/*
	local-declaration → object-definition
*/
Decl* Parser::parse_local_declaration()
{
	return parse_object_definition();
}

/*
	object-definition → variable-definition
		| constant-definition
		| value-definition
*/
Decl* Parser::parse_object_definition()
{
	switch(lookahead())
	{
		case tok_key_var:
			return parse_variable_definition();
		case tok_key_let:
			return parse_constant_definition();
		case tok_key_def:
			return parse_value_definition();
		default:
			throw std::runtime_error(getErrorString("parse object definition error"));
	}
}

bool Parser::peak_if_object_definition()
{
	switch(peak().getName())
	{
		case tok_key_var:
		case tok_key_let:
		case tok_key_def:
			return true;
	}
	return false;
}

/*
	variable-definition → var identifier :type ;
		| var identifier : type = expression ;
*/
Decl* Parser::parse_variable_definition()
{
	match(tok_key_var);
	Token tok = match(tok_identifier);
	match(tok_colon);
	Type* t = parse_type();
	if(!peak_if(tok_semicolon))
	{
		match(tok_assignment);
		Expr* e = parse_expression();
		match(tok_semicolon);

		return sem->on_variable_declaration(tok, e, t);
	}
	match(tok_semicolon);
	return sem->on_variable_declaration(tok, t);
}

/*
	constant-definition → let identifier : type = expression ;
*/
Decl* Parser::parse_constant_definition()
{
	match(tok_key_let);
	Token tok = match(tok_identifier);
	match(tok_colon);
	Type* t = parse_type();
	match(tok_assignment);
	Expr* e = parse_expression();
	match(tok_semicolon);
	return sem->on_constant_declaration(tok, e, t);
}

/*
	value-definition → def identifier : type = expression ;
*/
Decl* Parser::parse_value_definition()
{
	match(tok_key_def);
	Token tok = match(tok_identifier);
	match(tok_colon);
	Type* t = parse_type();
	match(tok_assignment);
	Expr* e = parse_expression();
	match(tok_semicolon);
	return sem->on_value_declaration(tok, e, t);
}

/*
	function-definition → def identifier ( parameter-list? ) -> type block-statement
*/
Decl* Parser::parse_function_definition()
{
	match(tok_key_def);
	Token tok = match(tok_identifier);
	match(tok_left_paren);
	sem->enterParameterScope();
	DeclList params;
	if(!peak_if(tok_right_paren))
	{
		params = parse_parameter_list();
	}
	match(tok_right_paren);
	match(tok_arrow);
	Type* t = parse_type();
	// Block Statement enters and exits it's own scope.
	Stmt* s = parse_block_statement();
	sem->exitScope();
	return sem->on_function_declaration(tok, params, s, t);
}

bool Parser::peak_if_function_definition()
{
	return peak_if(tok_key_def);
}

/*
	parameter-list → parameter-list , parameter
		| parameter	
*/
DeclList Parser::parse_parameter_list()
{
	DeclList dl;
	dl.push_back(parse_parameter());
	while(match_if_parameter_list())
		dl.push_back(parse_parameter());
	return dl;
}

bool Parser::match_if_parameter_list()
{
	return match_if(tok_comma);
}

/*
	parameter → identifier : type
*/
Decl* Parser::parse_parameter()
{
	Token tok = match(tok_identifier);
	match(tok_colon);
	Type* t = parse_type();
	return sem->on_parameter_declaration(tok, t);
}

// -- 2.1 Types --

/*
	type -> postfix-type
*/
Type* Parser::parse_type()
{
	return parse_postifx_type();
}

/*
	postfix-type → postfix-type *
		| postfix-type const
		| postfix-type volatile
		| postfix-type [ expression ]
		| postfix-type [ ]
		| basic-type
*/
Type* Parser::parse_postifx_type()
{
	Type* t1 = parse_basic_type();
	while(true)
	{
		switch(lookahead())
		{
			case tok_multiply:
			// case tok_key_const:
				t1 = sem->on_pointer_type(t1);
				accept();
				continue;
			// case tok_key_volatile:

			// // Arrays and idexes are not implemented
			// case tok_left_bracket:
			// 	match(tok_left_bracket);
			// 	if(!peak_if(tok_right_bracket))
			// 		parse_expression();
			// 	match(tok_right_bracket);
			// 	continue;
		}
		break;
	}
	return t1;
}

/*
	basic-type → void
		| bool
		| int
		| float
		| char
		| ( type-list? ) -> type
		| ( type )
*/
Type* Parser::parse_basic_type()
{
	TypeList tl = TypeList();
	Type* t;
	switch(lookahead())
	{
		case tok_key_void:
		case tok_key_bool:
		case tok_key_int:
		case tok_key_float:
		case tok_key_char:
			return sem->on_basic_type(accept()); 
		case tok_left_paren:
			if(lookahead(3) == tok_comma)
			{
				match(tok_left_paren);
				if(!peak_if(tok_right_paren))
					tl =parse_type_list();
				match(tok_right_paren);
				match(tok_arrow);
				t = parse_type();
				return sem->on_function_type(tl, t);
			}
			else
			{
				match(tok_left_paren);
				return parse_type();
				match(tok_right_paren);
			}
			break;
	}
}

/*
	type-list → type-list,type
		| type
*/
TypeList Parser::parse_type_list()
{
	TypeList tl = TypeList();
	tl.push_back(parse_type());
	while(match_if_type_list())
		tl.push_back(parse_type());
	return tl;
}
/*	ExprList el = ExprList();
	el.push_back(parse_argument());
	while(match_if_argument_list())
		el.push_back(parse_argument());
	return el;
*/

bool Parser::match_if_type_list()
{
	return match_if(tok_comma);
}

// /*
// 	reference-type → reference-type &
// 		| postfix-type
// */
// Type* Parser::parse_reference_type()
// {
// 	if(match_if_postfix_expression())
// 		return parse_postifx_type();
// 	else
// 	{
// 		Type* t = on_ref	();
// 		match(tok_bit_and);
// 		return t;
// 	}
// 	while(!match_if_postfix_expression())
// 	{
// 		Type* t = ();
// 		match(tok_bit_and);
// 		return t;
// 	}
// 	return parse_postifx_type();
// }