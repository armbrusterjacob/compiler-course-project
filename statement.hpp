#pragma once

#include <vector>
#include "expression.hpp"
#include "declaration.hpp"

class Decl;
class Expr;
class Stmt;
using StmtList = std::vector<Stmt*>;

enum stat_kind
{
	st_block,
	st_if,
	st_while,
	st_continue,
	st_break,
	st_statement,
	st_return,
	st_declaration,
	st_expression,
};

class Stmt
{
private:
	stat_kind sk;
public:

	Stmt(stat_kind s)
	{
		sk = s;
	}
	virtual ~Stmt() {}

	stat_kind getKind() const{ return sk;}
	void setKind(stat_kind s) {sk = s;}

	bool isBlock() const  {return sk == st_block;}
	bool isIf() const  {return sk == st_if;}
	bool isWhile() const  {return sk == st_while;}
	bool isContinue() const  {return sk == st_continue;}
	bool isBreak() const  {return sk == st_break;}
	bool isStatement() const  {return sk == st_statement;}
	bool isReturn() const  {return sk == st_return;}
	bool isDeclaration() const  {return sk == st_declaration;}
	bool isExpression() const  {return sk == st_expression;}
};

struct BlockStmt : Stmt
{
	// may need const/
	StmtList stat_list;

	BlockStmt(std::vector<Stmt*>& sl) : Stmt(st_block)
	{
		stat_list = sl;
	}

	StmtList getStmtList() const {return stat_list;}
};

struct IfStmt : Stmt
{
	Expr* condition;
	Stmt* on_true;
	Stmt* on_false;

	IfStmt(Expr* c, Stmt* ot, Stmt* of) : Stmt(st_if)
	{
		condition = c;
		on_true = ot;
		on_false = of;
	}

	IfStmt(Expr* c, Stmt* ot) : Stmt(st_if)
	{
		condition = c;
		on_true = ot;
		on_false = nullptr;
	}

	bool hasElse() const {return on_false;}
	Expr* getCond() const {return condition;}
	Stmt* getTrueStmt() const {return on_true;}
	Stmt* getFalseStmt() const {return on_false;}
};

struct WhileStmt : Stmt
{
	Expr* condition;
	Stmt* loop;

	WhileStmt(Expr* c, Stmt* l) : Stmt(st_while)
	{
		condition = c;
		loop = l;
	}

	Stmt* getLoop() const {return loop;}
	Expr* getCond() const {return condition;}
};

struct ContinueStmt : Stmt
{
	ContinueStmt() : Stmt(st_continue) {}
};

struct BreakStmt : Stmt
{
	BreakStmt() : Stmt(st_break) {}
};

struct ReturnStmt : Stmt
{
	Expr* expr;
	
	ReturnStmt() : Stmt(st_return) 
	{
		expr = nullptr;
	}
	
	ReturnStmt(Expr* e) : Stmt(st_return)
	{
		expr = e;
	}

	Expr* getExpr() const {return expr;}
	bool hasExpr() const {return expr;}
};

struct DeclarationStmt : Stmt
{
	Decl* decl;

	DeclarationStmt(Decl* d) : Stmt(st_declaration)
	{
		decl = d;
	}

	Decl* getDeclaration() const {return decl;}
};

struct ExpressionStmt : Stmt
{
	Expr* expr;

	ExpressionStmt(Expr* e) : Stmt(st_expression)
	{
		expr = e;
	}

	Expr* getExpr() const {return expr;}
};
