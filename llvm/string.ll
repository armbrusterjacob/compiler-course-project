define i32 @string_len(i8* %str)
{
	%len = alloca i32, i32 0
	%string =  alloca i8*
	store i8* %str, i8** %string
	br label %While

	While:
	%pointer = load i8*, i8** %string
	%char = load i8, i8* %pointer
	%cond = icmp ne i8 0, %char

	;getelementptr used to increment pointer
	%1 = getelementptr inbounds i8, i8* %pointer, i64 1
	store i8* %1, i8** %string
	%2 = load i32, i32* %len
	%3 = add i32 %2, 1
	store i32 %3, i32* %len

	br i1 %cond, label %While, label %Exit

	Exit:
	%4 = load i32, i32* %len
	%5 = add i32 %4, -1
	ret i32 %5
}


define i32 @string_cmp(i8* %lhs, i8* %rhs)
{
	%string1 =  alloca i8*
	%string2 =  alloca i8*
	store i8* %lhs, i8** %string1
	store i8* %rhs, i8** %string2
	br label %While

	While:
	%pointer1 = load i8*, i8** %string1
	%char1 = load i8, i8* %pointer1
	%pointer2 = load i8*, i8** %string2
	%char2 = load i8, i8* %pointer2

	%zeroCheck1 = icmp eq i8 %char1, 0
	%zeroCheck2 = icmp eq i8 %char2, 0
	%zeroCond = or i1 %zeroCheck1, %zeroCheck2
	br i1 %zeroCond , label %GreaterThan, label %CheckEqual

	CheckEqual:
	%equalCond = icmp eq i8 %char1, %char2
	br i1 %equalCond, label %Incrememnt, label %GreaterThan

	Incrememnt:
	%1 = getelementptr inbounds i8, i8* %pointer1, i64 1
	store i8* %1, i8** %string1
	%2 = getelementptr inbounds i8, i8* %pointer2, i64 1
	store i8* %2, i8** %string2
	br label %While

	GreaterThan:
	%greaterCond = icmp ugt i8 %char1, %char2
	br i1 %greaterCond, label %ReturnLHS, label %LessThan

	LessThan:
	%lessCond = icmp ult i8 %char1, %char2
	br i1 %lessCond, label %ReturnRHS, label %ReturnEqual

	ReturnLHS:
	ret i32 1
	ReturnRHS:
	ret i32 -1
	ReturnEqual:
	ret i32 0
}

define i8* @string_chr(i8* %str, i8 %ch)
{
	%string =  alloca i8*
	store i8* %str, i8** %string
	%last = alloca i8*
	store i8* null, i8** %last
	br label %While

	While:
	%pointer = load i8*, i8** %string
	%char = load i8, i8* %pointer
	%cond = icmp eq i8 %ch, %char
	br i1 %cond, label %NewLast, label %CheckEnd

	NewLast:
	store i8* %pointer, i8** %last
	br label %CheckEnd

	CheckEnd:
	%endCond = icmp eq i8 %char, 0
	br i1 %endCond, label %ReturnLast, label %Incrememnt

	Incrememnt:
	%1 = getelementptr inbounds i8, i8* %pointer, i64 1
	store i8* %1, i8** %string
	br label %While

	ReturnLast:
	%2 = load i8*, i8** %last
	ret i8* %2

}

define i8* @string_cpy(i8* %dest, i8* %src, i32 %count)
{
	%string1 =  alloca i8*
	%string2 =  alloca i8*
	store i8* %src, i8** %string1
	store i8* %dest, i8** %string2
	%i = alloca i32
	store i32 0, i32* %i
	br label %While

	While:
	%pointer1 = load i8*, i8** %string1
	%pointer2 = load i8*, i8** %string2
	%char = load i8, i8* %pointer1
	store i8 %char, i8* %pointer2
	%1 = load i32, i32* %i
	%2 = icmp ne i32 %1, %count
	%3 = icmp ne i8 %char, 0
	%cond = and i1 %2, %3

	;Increment everything
	%4 = load i32, i32* %i
	%5 = add i32 %4, 1
	store i32 %5, i32* %i
	%6 = getelementptr inbounds i8, i8* %pointer1, i64 1
	store i8* %6, i8** %string1
	%7 = getelementptr inbounds i8, i8* %pointer2, i64 1
	store i8* %7, i8** %string2

	br i1 %cond, label %While, label %Return

	Return:
	ret i8* %dest
}

;append src to dest
define i8* @string_cat(i8* %dest, i8* %src)
{
	%srcStr =  alloca i8*
	%destStr =  alloca i8*
	store i8* %src, i8** %srcStr
	store i8* %dest, i8** %destStr
	br label %FindEnd

	;find terminating charcter of dest
	FindEnd:
	%endptr = load i8*, i8** %destStr
	%endchar = load i8, i8* %endptr
	%endcond = icmp ne i8 0, %endchar
	%1 = getelementptr inbounds i8, i8* %endptr, i64 1
	store i8* %1, i8** %destStr
	br i1 %endcond, label %FindEnd, label %FoundEnd

	FoundEnd:
	;Overwrite first terminating charcter
	%2 = getelementptr inbounds i8, i8* %endptr, i64 0
	store i8* %2, i8** %destStr
	br label %While

	While:
	%pointer1 = load i8*, i8** %srcStr
	%pointer2 = load i8*, i8** %destStr
	%char = load i8, i8* %pointer1
	store i8 %char, i8* %pointer2
	%cond = icmp ne i8 %char, 0

	%3 = getelementptr inbounds i8, i8* %pointer1, i64 1
	store i8* %3, i8** %srcStr
	%4 = getelementptr inbounds i8, i8* %pointer2, i64 1
	store i8* %4, i8** %destStr
	br i1 %cond, label %While, label %Return

	Return:
	ret i8* %dest
}