#pragma once

#include <vector>
#include "expression.hpp"
#include "symbolTable.hpp"
#include "statement.hpp"
#include "type.hpp"

class Expr;
class Decl;
using DeclList = std::vector<Decl*>;

enum decl_kind
{
	dl_program,
	dl_function,
	//objects
	dl_variable,
	dl_cosntant,
	dl_value,
	// ? 
	dl_parameter,
};

class Decl
{
private:
	symbol symb;
	decl_kind decl;

public:
	Decl(decl_kind d)
	{
		decl = d;
	}

	Decl(decl_kind d, symbol s)
	{
		decl = d;
		symb = s;
	}
	virtual ~Decl() {} //for dynamic_cast

	decl_kind getKind() const {return decl;}
	void setKind(decl_kind d) {decl = d;}

	bool is_program() const;
	bool is_function() const;
	bool is_variable() const;
	bool is_cosntant() const;
	bool is_value() const;
	bool is_parameter() const;

	std::string getName() const;
};

struct ProgramDecl : Decl
{
	DeclList decl_list;
	ProgramDecl(DeclList& dl) : Decl(dl_program)
	{
		decl_list = dl;
	}

	DeclList getDeclList() const {return decl_list;}
};

// Decl with a type
struct TypedDecl : Decl
{
	Type* type;

	TypedDecl(decl_kind d, symbol s, Type* t) : Decl(d, s)
	{
		type = t;
	}	

	Type* getType() const {return type;}
	type_kind getTypeKind() const {return type->getKind();}
	void setType(Type* t) {type = t;}
};

struct FunctionDecl : TypedDecl
{
	DeclList params;
	Stmt* body;
	FunctionDecl(symbol s, Type* t, Stmt* st, DeclList& dl) 
	: TypedDecl(dl_function, s ,t)
	{
		params = dl;
		body = st;
	}

	DeclList getDeclList() const {return params;}
	Stmt* getBody() const {return body;}
};

// Provides Functions related to objects
struct ObjectDecl : TypedDecl
{
	Expr* expr;
	ObjectDecl(decl_kind d, symbol s, Type* t) : TypedDecl(d, s, t)
	{
		expr = nullptr;
	}
	ObjectDecl(decl_kind d, symbol s, Type* t, Expr* e) : TypedDecl(d, s, t)
	{
		expr = e;
	}

	Expr* getExpr() {return expr;}
	bool isExpr()
	{
		if(expr)
			return true;
		return false;
	}

};

struct VariableDecl : ObjectDecl
{
	VariableDecl(symbol s, Type* t) : ObjectDecl(dl_variable, s, t) {}
	VariableDecl(symbol s, Type* t, Expr* e) : ObjectDecl(dl_variable, s, t, e) {}
};

struct ConstantDecl : ObjectDecl
{
	ConstantDecl(symbol s, Type* t, Expr* e) : ObjectDecl(dl_cosntant, s, t, e) {}
};

struct ValueDecl : ObjectDecl
{
	ValueDecl(symbol s, Type* t, Expr* e) : ObjectDecl(dl_value, s, t, e) {}
};

struct ParameterDecl : ObjectDecl
{
	ParameterDecl(symbol s, Type* t) : ObjectDecl(dl_parameter, s, t) {}
};

