#pragma once

#include <string>
#include <unordered_set>

using symbol = const std::string*;

/** Symbol Table
	A data structure that stores symbols used in keywords, identifiers and strings.
*/
class SymbolTable
{
private:
	std::unordered_set<std::string> symbols;
public:
	SymbolTable();
	symbol get(const std::string& str);
};