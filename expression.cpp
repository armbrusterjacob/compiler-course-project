#include "expression.hpp"

bool Expr::isArithmic()
{
	return kind == ex_arithmic;
}

bool Expr::isInt()
{
	return kind == ex_int;
}

bool Expr::isFloat()
{
	return kind == ex_float;
}

bool Expr::isChar()
{
	return kind == ex_char;
}

bool Expr::isBool()
{
	return kind == ex_bool;
}

bool Expr::isIdentifer()
{
	return kind == ex_identifer;
}

bool Expr::isBinary()
{
	return kind == ex_binary;
}

bool Expr::isUnary()
{
	return kind == ex_unary;
}

bool Expr::isConditional()
{
	return kind == ex_conditional;
}

bool Expr::isCast()
{
	return kind == ex_cast;
}

bool Expr::isIndex()
{
	return kind == ex_index;
}

bool Expr::isConversion()
{
	return kind == ex_conversion;
}
bool Expr::isCall()
{
	return kind == ex_call;
}