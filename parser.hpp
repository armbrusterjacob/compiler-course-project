#pragma once

#include <iostream>
#include <deque>
#include <string>
#include "lexer.hpp"
#include "token.hpp"
#include "expression.hpp"
#include "type.hpp"
#include "statement.hpp"
#include "declaration.hpp"
#include "semantics.hpp"

class Parser
{
private:
	Lexer* lexer;
	Semantics* sem;
	std::deque<Token> tokenQ;
public:
	Parser(const std::string& file);

	tokenName lookahead();
	std::string lookaheadName();
	std::string getErrorString(std::string text);
	tokenName lookahead(const int& num);
	Token match(tokenName tok);
	bool match_if(tokenName tok);
	Token accept();
	Token peak();
	bool peak_if(tokenName tok);
	void fetch();
	Lexer* getLexer() {return lexer;}
	SymbolTable* getSymbolTable() {return lexer->getSymbolTable();}

	// --2.1 Types --

	Type* parse_type();
	Type* parse_postifx_type();
	Type* parse_basic_type();
	TypeList parse_type_list();
	Type* parse_reference_type();

	// -- 2.2 Expressions --

	Expr* parse_primary_expression();
	Expr* parse_postfix_expression();
	ExprList parse_argument_list();
	Expr* parse_argument();
	Expr* parse_unary_expression();
	Expr* parse_cast_expression();
	Expr* parse_multiplicative_expression();
	Expr* parse_additive_expression();
	Expr* parse_shift_expresion();
	Expr* parse_reltional_expression();
	Expr* parse_equality_expression();
	Expr* parse_bitwise_and_expression();
	Expr* parse_bitwise_xor_expression();
	Expr* parse_bitwise_or_expression();
	Expr* parse_logical_and_expression();
	Expr* parse_logical_or_expression();
	Expr* parse_conditional_expression();
	Expr* parse_assignment_expression();
	Expr* parse_expression();
	Expr* parse_constant_expression();

	// -- 2.3 Statements --

	Stmt* parse_statement();
	Stmt* parse_block_statement();
	StmtList parse_statement_sequence();
	Stmt* parse_if_statment();
	Stmt* parse_while_statement();
	Stmt* parse_break_statement();
	Stmt* parse_continue_statement();
	Stmt* parse_return_statement();
	Stmt* parse_declaration_statment();
	Stmt* parse_expression_statement();

	// -- 2.4 Declarations --

	Decl* parse_program();
	Decl* parse_declaration();
	DeclList parse_declaration_sequence();
	Decl* parse_local_declaration();
	Decl* parse_object_definition();
	Decl* parse_variable_definition();
	Decl* parse_constant_definition();
	Decl* parse_value_definition();
	Decl* parse_function_definition();
	DeclList parse_parameter_list();
	Decl* parse_parameter();


	// -- Match and Peak Functions --

	bool match_if_cast();
	bool match_if_postfix_expression();
	bool match_if_conditional();
	bool match_if_assigment();
	bool match_if_argument_list();
	bool match_if_parameter_list();
	bool match_if_type_list();

	bool peak_if_primary();
	bool peak_if_unary();
	bool peak_if_multiplacative();
	bool peak_if_additive();
	bool peak_if_shift();
	bool peak_if_relational();
	bool peak_if_equality();
	bool peak_if_bitwise_and();
	bool peak_if_bitwise_xor();
	bool peak_if_bitwise_or();
	bool peak_if_logical_or();
	bool peak_if_logical_and();
	bool peak_if_expression_statement();
	bool peak_if_function_definition();
	bool peak_if_object_definition();
	bool peak_if_declaration();
	bool peak_if_statement();
	// bool match_if_

};