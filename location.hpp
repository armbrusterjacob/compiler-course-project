#pragma once

#include <string>

/** Location
	A Location is a an object that points to a specific point in a file.
*/
class Location
{
private:
	std::string *file;
	unsigned int column;
	unsigned int line;
public:
	Location(std::string& str);
	Location(const Location& l);
	~Location()
	{
		return;
	}
	int getColumn();
	int getLine();
	std::string* getFile();
	void newLine();
	void advanceColumn(const int& k);

	std::string toString();
};