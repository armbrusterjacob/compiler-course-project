#include <string>
#include <cctype>
#include <vector>
#include <iostream>
#include "lexer.hpp"
#include "symbolTable.hpp"

/** 
	Constrcutor
		A constructor that takes a string as input. Sets iterators, builds symbol table.
*/
Lexer::Lexer(const std::string& fileStr)
{
	file = fileStr;
	wordLength = 0;
	wordStart = file.begin();
	current = wordStart;
	eofIt = file.end();
	loc = new Location(file);

	reserved.insert({
    {"as", tok_key_as},
    {"and", tok_key_and},
    {"bool", tok_key_bool},
    {"break", tok_key_break},
    {"char", tok_key_char},
    {"const", tok_key_const},
    {"continue", tok_key_continue},
    {"def", tok_key_def},
    {"else", tok_key_else},
    {"false", tok_key_false},
    {"float", tok_key_float},
    {"if", tok_key_if},
    {"int", tok_key_int},
    {"let", tok_key_let},
    {"not", tok_key_not},
    {"or", tok_key_or},
    {"return", tok_key_return},
    {"true", tok_key_true},
    {"var", tok_key_var},
    {"void", tok_key_void},
    {"volatile", tok_key_volatile},
    {"while", tok_key_while},
	});

	symT = new SymbolTable();
	symT->get("as");
	symT->get("bool");
	symT->get("break");
	symT->get("char");
	symT->get("const");
	symT->get("continue");
	symT->get("def");
	symT->get("else");
	symT->get("false");
	symT->get("float");
	symT->get("if");
	symT->get("int");
	symT->get("let");
	symT->get("not");
	symT->get("or");
	symT->get("return");
	symT->get("true");
	symT->get("var");
	symT->get("void");
	symT->get("volatile");
	symT->get("while");

	return;
}

/**
	lexFile()
		Continuously both lexes and adds those tokens to a vector
*/
void Lexer::lexFile()
{
	while(!eof())
	{
		tokenVector.push_back(lex());
	}
}

/**
	lex()
		Lex() determines what kind of token the next sequence of charcters are as
		long as the input is not at end of file. Lex does this by reading in both
		the current character using *current, and the next character using peak().
		In addition any character not related to tokens like newlinse and whitespace
		are skiped with ignore(). Tokens are then handled by either lexGeneral(), 
		lexWord(), or lexNumber().
*/
Token Lexer::lex()
{

	while(!eof())
	{
		switch(*current)
		{
			case '\n':
				ignore();
				newLine();
				break;
			case ' ':
			case '\r':
			case '\t':
			case '\v':
				ignore(); break;
			case '#':
				ignore();
				while(*current != '\n' && !eof())
				{
					ignore();
				}
				break;
			case '{':
				return lexGeneral(1,tok_left_brace); 
			case '}':
				return lexGeneral(1,tok_right_brace);
			case '(':
				return lexGeneral(1,tok_left_paren);
			case ')':
				return lexGeneral(1,tok_right_paren);
			case '[':
				return lexGeneral(1,tok_left_bracket);
			case ']':
				return lexGeneral(1,tok_right_bracket);
			case ',':
				return lexGeneral(1,tok_comma);
			case ';':
				return lexGeneral(1,tok_semicolon);
			case ':':
				return lexGeneral(1,tok_colon);
			case '=':
				if(peak() == '=')
					return lexGeneral(2,tok_equals);
				else
					return lexGeneral(1,tok_assignment);
			case '!':
				if(peak() == '=')
					return lexGeneral(2,tok_not_equals);
				return lexGeneral(1,tok_log_not);
			case '<':
				if(peak() == '<')
					return lexGeneral(2,tok_shift_left);
				if(peak() == '=')
					return lexGeneral(2,tok_less_than_equals);
				return lexGeneral(1,tok_less_than);
			case '>':
				if(peak() == '>')
					return lexGeneral(2,tok_shift_right);
				if(peak() == '=')
					return lexGeneral(2,tok_greater_than_equals);
				return lexGeneral(1,tok_greater_than);
			case '+':
				return lexGeneral(1,tok_plus);
			case '-':
				if(peak() == '>')
					return lexGeneral(2, tok_arrow);
				return lexGeneral(1,tok_minus);
			case '*':
				return lexGeneral(1,tok_multiply);
			case '/':
				return lexGeneral(1,tok_divide);
			case '%':
				return lexGeneral(1,tok_mod);
			case '&':
				if(peak() == '&')
					return lexGeneral(2,tok_log_and);
				return lexGeneral(1,tok_bit_and);
			case '|':
				if(peak() == '|')
					return lexGeneral(2,tok_log_or);
				return lexGeneral(1,tok_bit_or);
			case '^':
				return lexGeneral(1,tok_bit_xor);
			case '~':
				return lexGeneral(1,tok_bit_not);
			case '?':
				return lexGeneral(1,tok_conditional);
			default:
				if(isdigit(*current))
					return lexNumber();
				if(isalpha(*current) || *current == '_')
					return lexWord();

				throw;
				//No legal characters should make it here
		}
	}
	if(eof())
	{
		return lexEof();
	}
}

Token Lexer::lexEof()
{
	return Token(tok_eof, *loc);
}

/**
	lexGeneral()
		LexGeneral accepts the charcter(s) of a token then creates a token of
		just a token name and location. 
*/
Token Lexer::lexGeneral(const int& length, tokenName name)
{
	resetWord();
	accept(length);
	return Token(name, *loc);
}

/**
	lexWord()
		LexWord() first accepts all either alphanumeric charactgers or underscores.
		Then after constructing a string of those characters it determines if it's
		true, false, or a keyword. If it's none of those things then it's an identifier.
*/
Token Lexer::lexWord()
{
	resetWord();
	accept();
	while(!eof() && isalnum(*current) || *current == '_')
	{
		accept();
	}
	//check if the buffer is a keyword
	std::string word = std::string(wordStart, current);
	if(word == "false")
		return Token(tok_bool, false, *loc);
	if(word == "true")
		return Token(tok_bool, true, *loc);
	auto it = reserved.find(word);
	if(it != reserved.end())
	{
		return Token(it->second, *loc);
	}
	//now that it can't be a keyword it's an identifier
	//check if the identifier already exists, if not add it
	symbol sym = symT->get(word);

	return Token(tok_identifier, sym, *loc);
}

/**
	lexNumber()
		LexNumber() first determines if the number is either in binary or hexidecimal
		by looking for "0b" or "0x" patterns. If they are, they're sent off. Then all
		numerical characters are added to a string while also checking for floating
		points. Finally if it's not a floating point its a decimal and the string is 
		converted into an int and the token is made with that decimal as an attribute.
*/
Token Lexer::lexNumber()
{
	resetWord();
	if(*current == '0')
	{
		if(!eof() && peak() == 'x' || peak() == 'X')
		{
			return lexHex();
		}else if(!eof() && peak() == 'b' || peak() == 'B')
		{
			return lexBin();
		}
	}
	do
	{
		accept();
		if(!eof() && *current == '.')
		{
			return lexFloat();
		}
	}while(!eof() && isdigit(*current));
	//buffer is now an int
	std::string word =  std::string(wordStart, current);
	int num = std::stoi(word, nullptr, 10);
	return Token(tok_dec_int, num, *loc);
}

/**
	lexHex()
		LexHex() accepts all hexidecimal charcters past the "0x" portion of the
		string then converts it into a hex int. Then a token is created using the
		hex int as an attribute.
*/
Token Lexer::lexHex()
{
	accept(2); //accept 0, then x or X
	while(!eof() && isxdigit(*current))
	{
		accept();
	}
	//buffer should now be 0x[some hex number]
	std::string word =  std::string(wordStart, current);
	std::string hexStr = word.substr(2);
	int hexInt = std::stoi(hexStr, nullptr, 16);

	return Token(tok_hex_int, hexInt, *loc);
}


/**
	lexzBin()
		LexBin() accepts all hexidecimal charcters past the "0b" portion of the
		string then converts it into a binary int. Then a token is created using the
		binary int as an attribute.
*/
Token Lexer::lexBin()
{
	accept(2); //accept 0, then b or B
	while(!eof() && *current == '0' || *current == '1')
	{
		accept();
	}
	//buffer should now be 0b[some binary number
	std::string word =  std::string(wordStart, current);

	std::string binStr = word.substr(2);
	int binInt = std::stoi(binStr, nullptr, 2);

	return Token(tok_bin_int, binInt, *loc);
}

/**
	lexFloat()
		LexFloat() accepts all digits past the decimal from the previous function, 
		and then converts those charcters into a float which is used as the 
		attribute for the token.
*/
Token Lexer::lexFloat()
{
	accept(); //accept .
	while(!eof() && isdigit(*current))
	{
		accept();
	}
	//buffer is now [num].[num]
	std::string word =  std::string(wordStart, current);
	float num = std::stof(word);

	return Token(tok_float, num, *loc);
}

/**
	eof()
		Checks if the current iterator is at the end of the file.
*/
bool Lexer::eof()
{
	if(current == eofIt)
		return true;
	else
		return false;
}

/**
	accept()
		Advances the lexer by a single charcter by incrementing iterator.
*/
void Lexer::accept()
{
	current++;
	wordLength++;
	return;
}

/**
	accept()
		Advances the lexer by num amount of characters by incrementing iterator.
*/
void Lexer::accept(const int& num)
{
	int k = num;
	while(!eof() && k-- > 0)
		current++;
	wordLength += num;
	return;
}

/**
	ignore()
		Advances the lexer one characeter without changing word length.
*/
void Lexer::ignore()
{
	current++;
	return;
}

/**
	ignore()
		Advances the lexer num charcters without changing word length.
*/
void Lexer::ignore(const int& num)
{
	int k = num;
	while(!eof() && k-- == 0)
		current++;
	return;
}

/**
	peak()
		Returns the next charcter from the lexer without advancing.
*/
char Lexer::peak()
{
	current++;
	if(!eof())
	{
		char c = *current;
		current--;
		return c; 
	}
	current--;
	return '\0';
	//not really sure what to do if I peek after eof...
}

/**
	resetWord()
		Sets the start of the word to the start of a token and resets
		loc to that file location. Word length is also reset.
*/
void Lexer::resetWord()
{
	wordStart = current;
	loc->advanceColumn(wordLength);
	wordLength = 0;
	return;
}

/**
	newLine()
		Sets loc to the next line.
*/
void Lexer::newLine()
{
	loc->newLine();
	wordLength = 0;
	return;
}

/**
	printToken()
		Prints all lexed tokens in the order they appeared on their line by
		using locations.
*/
void Lexer::printTokens()
{
	auto itCurr = tokenVector.begin();
	auto itEnd = tokenVector.end();
	int line = 0;
	while (itCurr != itEnd)
	{
		while(line != itCurr->getLine())
		{
			std::cout << std::endl;
			line++;
		}
		std::cout << itCurr->toString() << " ";
		itCurr++;
	}
	std::cout << std::endl;
	return;
}