#include <string>
#include "symbolTable.hpp"

/**
	Constructor
*/
SymbolTable::SymbolTable()
{
	return;
}

/**
	get()
		Adds strings to the symbol vector and returns a symbol. Returns a pointer
		to the where the element is inserted or exists if it was alreay inserted.
*/
symbol SymbolTable::get(const std::string& str)
{
	auto a = symbols.insert(str);
	return &(*a.first);
}