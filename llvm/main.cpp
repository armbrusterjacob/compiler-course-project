#include <stdio.h>
#include <iostream>

extern "C" int string_len(const char* str);
extern "C" int string_cmp(const char* str1, const char* str2);
extern "C" const char* string_chr(const char* str, char c);
extern "C" char* string_cpy(char* dest, const char* src, int count);
extern "C" char* string_cat(char* dest, const char* src);

char foo(char* str)
{
	*(str + 1) = '3';
	return 'd';
}

int main() 
{
	//string_len
	char c[] = "Hello World!";
	std::cout << "-- string_len --" << std::endl;
	std::cout << "str  : " << c << std::endl;
	std::cout << "count: " << string_len(c) << std::endl << std::endl;

	//string_cmp
	char c1[] = "abc";
	char c2[] = "abc";
	char c3[] = "def";
	std::cout << "-- string_cmp --" << std::endl;
	std::cout << "cmp(abc,abc): " << string_cmp(c1,c2) << std::endl;
	std::cout << "cmp(abc,def): " << string_cmp(c1,c3) << std::endl;
	std::cout << "cmp(def,abc): " << string_cmp(c3,c1) << std::endl << std::endl;

	//string_chr
	char c4[] = "Hello World!";
	char c5 = 'l';
	std::cout << "-- string_chr --" << std::endl;
	std::cout << "str    : " << c4 << std::endl;
	std::cout << "last \'l\': " << string_chr(c4, c5) << std::endl << std::endl;

	//string _cpy
	char c6[] = "Hello World!";
	char c7[10];
	std::cout << "-- string_cpy --" << std::endl;
	std::cout << "src         : " << c6 <<std::endl;
	std::cout << "cpy, count=5: " << string_cpy(c7, c6, 5) << std::endl << std::endl;

	//string_cat
	char c9[30] = "Hello, ";
	char c10[30] = "cat.";
	std::cout << "-- string_cat --" << std::endl;
	std::cout << "dest  : " << c9 <<std::endl;
    std::cout << "src   : " << c10 <<std::endl;
	std::cout << "Return: " << string_cat(c9, c10) << std::endl << std::endl;

}

