#include <sstream>
#include <iostream>
#include "semantics.hpp"
#include "expression.hpp"
#include "type.hpp"
#include "statement.hpp"
#include "declaration.hpp"
#include "token.hpp"
#include "scope.hpp"

// -- 2.2 Expressions --

Expr* Semantics::on_cast_expression(Expr* e, Type* t)
{
	return new CastExpr(e, t);
}

Expr* Semantics::on_multiplicative_expression(Expr* e1, Expr* e2, Token tok)
{
	e1 = requireArithmetic(e1);
	e2 = requireArithmetic(e2);
	requireSame(e1, e2);
	bianry_operator_kind bn;
	switch(tok.getName())
	{
		case tok_multiply:
			bn = bn_multiply;
			break;
		case tok_divide:
			bn = bn_divide;
			break;
		case tok_mod:
			bn = bn_mod;
			break;

		default:
			throwLocationError("Undefined multiplicative token.", tok);
	}
	return new BinaryExpr(bn, e1, e2, e1->getType());
}

Expr* Semantics::on_additive_expression(Expr* e1, Expr* e2, Token tok)
{
	e1 = requireArithmetic(e1);
	e2 = requireArithmetic(e2);
	requireSame(e1, e2);
	bianry_operator_kind bn;
	switch(tok.getName())
	{
		case tok_plus:
			bn = bn_add;
			break;
		case tok_minus:
			bn = bn_subtract;
			break;

		default:
			throwLocationError("Undefined additive token.", tok);
	}
	return new BinaryExpr(bn, e1, e2, e1->getType());
}

Expr* Semantics::on_shift_expression(Expr* e1, Expr* e2, Token tok)
{
	e1 = requireInt(e1);
	e2 = requireInt(e2);
	switch(tok.getName())
	{
		case tok_shift_right:
			return new BinaryExpr(bn_bit_shift_right, e1, e2, ty_int);
		case tok_shift_left:
			return new BinaryExpr(bn_bit_shift_left, e1, e2, ty_int);
		default:
			throwLocationError("Undefined shift expression.", tok);
	}
	
}

Expr* Semantics::on_relational_expression(Expr* e1, Expr* e2, Token tok)
{
	e1 = ConvertToValue(e1);
	e2 = ConvertToValue(e2);
	bianry_operator_kind bn;
	switch(tok.getName())
	{
		case tok_less_than:
			bn = bn_less_than; break;
		case tok_less_than_equals:
			bn = bn_less_than_equals; break;
		case tok_greater_than:
			bn = bn_greater_than; break;
		case tok_greater_than_equals:
			bn = bn_greater_than_equals; break;
		default:
			throwLocationError("Undefined relational expression.", tok);
	}
	return new BinaryExpr(bn, e1, e2, ty_bool);
}

Expr* Semantics::on_equality_expression(Expr* e1, Expr* e2, Token tok)
{
	e1 = ConvertToValue(e1);
	e2 = ConvertToValue(e2);
	bianry_operator_kind bn;
	switch(tok.getName())
	{
		case tok_equals:
			bn = bn_equality; break;
		case tok_not_equals:
			bn = bn_inequality; break;
		default:
			throwLocationError("Undefined equality expression.", tok);
	}
	return new BinaryExpr(bn, e1, e2, ty_bool);
}

Expr* Semantics::on_bitwise_expression(Expr* e1, Expr* e2, Token tok)
{
	e1 = ConvertToInt(e1);
	e2 = ConvertToInt(e2);
	bianry_operator_kind bn;
	switch(tok.getName())
	{
		case tok_bit_xor:
			bn = bn_bit_xor; break;
		case tok_bit_and:
			bn = bn_bit_and; break;
		case tok_bit_or:
			bn = bn_bit_or; break;
		default:
			throwLocationError("Undefined bitwise expression.", tok);
	}
	return new BinaryExpr(bn, e1, e2, ty_int);
}

Expr* Semantics::on_logical_expression(Expr* e1, Expr* e2, Token tok)
{
	e1 = ConvertToBool(e1);
	e2 = ConvertToBool(e2);
	bianry_operator_kind bn;
	switch(tok.getName())
	{
		case tok_key_and:
			bn = bn_log_and; break;
		case tok_key_or:
			bn = bn_log_or; break;
	}
	return new BinaryExpr(bn, e1, e2, ty_bool);
}

Expr* Semantics::on_conditional_expression(Expr* e1, Expr* e2, Expr* e3)
{
	e1 = ConvertToBool(e1);
	requireSame(e2, e3);
	return new ConditionalExpr(e1, e2, e3);
}

Expr* Semantics::on_assignment_expression(Expr* lhs, Expr* rhs)
{
	lhs = requireReference(lhs);
	IdExpr* id = static_cast<IdExpr*>(lhs);
	rhs = ConvertToType(rhs, lhs->getType());
	return new BinaryExpr(bn_assignment, rhs, lhs, rhs->getType());
}

Expr* Semantics::on_int_literal(Token tok)
{
	int val = tok.getAtr().in;
	return new IntExpr(val);		
}

Expr* Semantics::on_float_literal(Token tok)
{
	float val = tok.getAtr().fl;
	return new FloatExpr(val);	
}

Expr* Semantics::on_char_literal(Token tok)
{
	char val = tok.getAtr().ch;
	return new CharExpr(val);	
}

Expr* Semantics::on_bool_literal(Token tok)
{
	bool val = tok.getAtr().bo;
	return new BoolExpr(val);	
}

Expr* Semantics::on_identifier(Token tok)
{
	symbol sym = tok.getAtr().sy;
	Decl* d = curr_scope->lookupRecursive(sym);
	if(!d)
	{
		std::string str =  tok.toString();
		throwLocationError("Identifier not found: " + tok.toString(), tok);
	}

	Type* t;
	TypedDecl* td = dynamic_cast<TypedDecl*>(d); 
	if(d->is_variable())
	{
		t = new ReferenceType(td->getType());
	}
	else
	{
		t = td->getType();
	}

	return new IdExpr(d, t, sym); 
}

// A call expression is composed of the base, and the arguments passed to the function.
// These parameters have types and so we need a type list of them.
Expr* Semantics::on_function_call(Expr* e, ExprList& args)
{	
	e = requireCall(e);

	CallType* cl = dynamic_cast<CallType*>(e->getType());
	TypeList params = cl->getTypeList();

	// Test call for mismatches between types and amount of types.
	if(params.size() > args.size())
		throw std::runtime_error("There are more parameters than arguemnts for this call\n");
	if(params.size() > args.size())
		throw std::runtime_error("There are more arguemnts than parameters for this call\n");

	for(int i = 0; i < args.size(); i++)
	{
		args.at(i) = ConvertToType(args.at(i), params.at(i));
		if(params.at(i)->getKind() != args.at(i)->getTypeKind())
			throw std::runtime_error("Types don't match in this call.");
	}

	return new CallExpr(e, args, cl);
}

Expr* Semantics::on_index(Expr* e, ExprList& args)
{
	throw std::runtime_error("Indexes are not implemented for this compiler because arrays aren't.\n");
}

Expr* Semantics::on_unary(Token tok, Expr* e)
{
	unary_operator_type un;
	switch(tok.getName())
	{
		case tok_plus:
			e = requireArithmetic(e);
			un = un_add;
			break;
		case tok_minus:
			e = requireArithmetic(e);
			un = un_subtract;
			break;
		case tok_bit_not:
			e = ConvertToInt(e);
			un = un_bit_not;
			break;
		case tok_log_not:
		case tok_key_not:
			e = ConvertToBool(e);
			un = un_log_not;
			break;
		case tok_bit_and:
			e = ConvertToReference(e);
			un = un_reference;
			break;
		case tok_multiply:
			e = ConvertToValue(e);
			un = un_dereference;
			break;
		default:
			throw std::runtime_error("Expression was assumed to be bool.\n");
	}

	return new UnaryExpr(un, e);

}

/*
	-- Types --
*/

Type* Semantics::on_basic_type(Token tok)
{
	type_kind tk;
	switch(tok.getName())
	{
		case tok_key_void:
			return new VoidType();
		case tok_key_bool:
			return new BoolType();
		case tok_key_int:
			return new IntType();
		case tok_key_float:
			return new FloatType();
		case tok_key_char:
			return new CharType();
		default:
			throwLocationError("No recognized type for token\n", tok);
	}
}

Type* Semantics::on_function_type(TypeList& tl, Type* t)
{
	return new CallType(tl, t);
}

Type* Semantics::on_pointer_type(Type* t)
{
	return new PointerType(t);
}



/*
	-- Statements --
*/

Stmt* Semantics::on_block_statement(StmtList& sl)
{
	return new BlockStmt(sl);
}

Stmt* Semantics::on_if_statement(Expr* e, Stmt* s1, Stmt* s2)
{
	e = ConvertToBool(e);
	return new IfStmt(e, s1, s2);
}

Stmt* Semantics::on_if_statement(Expr* e, Stmt* s)
{
	e = ConvertToBool(e);
	return new IfStmt(e, s);
}

Stmt* Semantics::on_while_statement(Expr* e, Stmt* s)
{
	e = ConvertToBool(e);
	return new WhileStmt(e, s);
}

Stmt* Semantics::on_continue_statement()
{
	return new ContinueStmt();
}

Stmt* Semantics::on_break_statement()
{
	return new BreakStmt();
}

Stmt* Semantics::on_return_statement()
{
	return new ReturnStmt();
}

Stmt* Semantics::on_return_statement(Expr* e)
{
	return new ReturnStmt(e);
}

Stmt* Semantics::on_declaration_statement(Decl* d)
{
	return new DeclarationStmt(d);
}

Stmt* Semantics::on_expression_statement(Expr* e)
{
	return new ExpressionStmt(e);
}

/*
	-- Declaration -- 
*/

Decl* Semantics::on_program_declaration(DeclList& dl)
{
	return new ProgramDecl(dl); 
}

Decl* Semantics::on_variable_declaration(Token tok, Expr* e, Type* t)
{
	e = ConvertToType(e, t);
	Decl* d = new VariableDecl(tok.getAtr().sy, t, e);
	declareToScope(tok, d);
	return d; 
}

Decl* Semantics::on_variable_declaration(Token tok, Type* t)
{
	Decl* d = new VariableDecl(tok.getAtr().sy, t);
	declareToScope(tok, d);
	return d; 
}

Decl* Semantics::on_constant_declaration(Token tok, Expr* e, Type* t)
{
	Decl* d = new ConstantDecl(tok.getAtr().sy, t, e);
	declareToScope(tok, d);
	return d; 
}

Decl* Semantics::on_value_declaration(Token tok, Expr* e, Type* t)
{
	Decl* d = new ValueDecl(tok.getAtr().sy, t, e);
	declareToScope(tok, d);
	return d; 
}

Decl* Semantics::on_function_declaration(Token tok, DeclList& params, Stmt* s, Type* t)
{
	TypeList tl;
	for(auto it = params.begin(); it != params.end(); it++)
	{
		ParameterDecl* pd = dynamic_cast<ParameterDecl*>(*it);
		tl.push_back(pd->getType());
	}
	Type* call_type = new CallType(tl, t);

	Decl* d = new FunctionDecl(tok.getAtr().sy, call_type, s, params);
	declareToScope(tok, d);
	return d; 
}

Decl* Semantics::on_parameter_declaration(Token tok, Type* t)
{
	Decl* d = new ParameterDecl(tok.getAtr().sy, t);
	declareToScope(tok, d);
	return d; 
}

/*
	-- Scope Functions --
*/

void Semantics::declareToScope(Token tok, Decl* d)
{
	symbol sym = tok.getAtr().sy;
	if(curr_scope->lookup(sym))
	{
		throwLocationError("Redeclaration", tok);
	}
	curr_scope->addToScope(sym, d);
}

void Semantics::enterGlobalScope()
{
	curr_scope = new GlobalScope();
	global_scope = curr_scope;
	return;
}

void Semantics::enterParameterScope()
{
	Scope* ps = new ParameterScope(curr_scope);
	curr_scope = ps;
	return;
}

void Semantics::enterBlockScope()
{
	Scope* bs = new BlockScope(curr_scope);
	curr_scope = bs;
	return;
}

void Semantics::exitScope()
{
	if(!curr_scope->getParent())
		throw std::runtime_error("Attempting to leave global scope\n");
	Scope* temp = curr_scope;
	curr_scope = curr_scope->getParent();
	delete temp;
	return;
}

Scope* Semantics::getCurrentScope()
{
	return curr_scope;
}

Scope* Semantics::getGlobalScope()
{
	return global_scope;
}



/* 
	-- ect --
*/

void Semantics::throwSymbolError(std::string str, symbol sym)
{
	std::stringstream ss;
	ss << str << sym << "\n";
	throw std::runtime_error(ss.str());
}

void Semantics::throwLocationError(std::string str, Token tok)
{
	std::string error = str; 
	str += + " ";
	str += tok.getLoc().toString();
	throw std::runtime_error(str);
}

/* -- Requirements -- 
        Each function makes sure the type* passed in has a certain type or else
        it calls an exception.
*/

Expr* Semantics::requireBool(Expr* e)
{
	e = ConvertToValue(e);
	if(e->getType()->isBool())
		return e;
	throw std::runtime_error("Expression was assumed to be bool.\n");
}

Expr* Semantics::requireChar(Expr* e)
{
	e = ConvertToValue(e);
	if(e->getType()->isChar())
		return e;
	throw std::runtime_error("Expression was assumed to be char.\n");
}

Expr* Semantics::requireInt(Expr* e)
{
	e = ConvertToValue(e);
	if(e->getType()->isInt())
		return e;
	throw std::runtime_error("Expression was assumed to be int.\n");
}

Expr* Semantics::requireFloat(Expr* e)
{
	e = ConvertToValue(e);
	if(e->getType()->isFloat())
		return e;
	throw std::runtime_error("Expression was assumed to be float.\n");
}

Expr* Semantics::requirePointer(Expr* e)
{
	if(e->getType()->isPointer())
		return e;
	throw std::runtime_error("Expression was assumed to be pointer.\n");
}

Expr* Semantics::requireReference(Expr* e)
{
	if(e->getType()->isReference())
		return e;
	throw std::runtime_error("Expression was assumed to be reference.\n");
}

Expr* Semantics::requireCall(Expr* e)
{
	if(e->getType()->isCall())
		return e;
	throw std::runtime_error("Expression was assumed to be call.\n");
}

Expr* Semantics::requireArithmetic(Expr* e)
{
	e = ConvertToValue(e);
	if(e->getType()->isInt() || e->getType()->isFloat())
		return e;
	throw std::runtime_error("Expression was assumed to be arithmetic.\n");
} 

Type* Semantics::requireSame(Expr*& e1, Expr*& e2)
{
	Type* t1 = e1->getType();
	Type* t2 = e2->getType();

	if(t1->isTyped())
	{
		TypedType* tt1 = dynamic_cast<TypedType*>(t1);
		t1 = tt1->getBaseType();
	}	
	if(t2->isTyped())
	{
		TypedType* tt2 = dynamic_cast<TypedType*>(t2);
		t2 = tt2->getBaseType();
	}

	if(t1->getKind() == t2->getKind())
		return t1;

	// Figure out highest type
	type_kind highest = ty_bool;
	if(t1->isChar() || t2->isChar())
		highest = ty_char;
	if(t1->isInt() || t2->isInt())
		highest = ty_int;
	if(t1->isFloat()|| t2->isFloat())
		highest = ty_float;

	// Convert both exprs to highest type.
	switch(highest)
	{
		case ty_bool:
			e1 = ConvertToBool(e1);
			e2 = ConvertToBool(e2);
			break;
		case ty_char:
			e1 = ConvertToChar(e1);
			e2 = ConvertToChar(e2);
			break;
		case ty_int:
			e1 = ConvertToInt(e1);
			e2 = ConvertToInt(e2);
			break;
		case ty_float:
			e1 = ConvertToFloat(e1);
			e2 = ConvertToFloat(e2);
			break;
	}

	return t1;

	// Shouldn't run?
	throw std::runtime_error("Expressions were assumed to be the same type.\n");
}


/*  -- Conversions --
         Convert an expression to a conversionExpr
*/


Expr* Semantics::ConvertToBool(Expr* e)
{
	e = ConvertToValue(e);

	switch(e->getTypeKind())
	{
		case ty_bool:
			return e;
		case ty_char:
		case ty_int:
		case ty_float:
		case ty_pointer:
			return new ConversionExpr(e, co_bool, ty_bool);
		case ty_call:
			return new ConversionExpr(e, co_bool, ty_bool);
		default:
			throw std::runtime_error("Cannot convert the expression to Bool type.\n");
	}
}

Expr* Semantics::ConvertToChar(Expr* e)
{
	e = ConvertToValue(e);

	switch(e->getTypeKind())
	{
		case ty_char:
			return e;
		case ty_int:
			return new ConversionExpr(e, co_char, ty_char);
		case ty_call:
			return new ConversionExpr(e, co_char, ty_char);
		case ty_pointer:
			return ConvertToValue(e);
		default:
			throw std::runtime_error("Cannot convert the expression to Char type.\n");
	}
}

Expr* Semantics::ConvertToInt(Expr* e)
{
	e = ConvertToValue(e);

	switch(e->getTypeKind())
	{
		case ty_bool:
		case ty_char:
			return new ConversionExpr(e, co_int, ty_int);
		case ty_int:
			return e;
		case ty_float:
			return new ConversionExpr(e, co_trunaction, ty_int);
		case ty_call:
			return new ConversionExpr(e, co_int, ty_int);
		case ty_pointer:
			return ConvertToValue(e);
		default:
			throw std::runtime_error("Cannot convert the expression to Int type.\n");
	}
}

Expr* Semantics::ConvertToFloat(Expr* e)
{
	e = ConvertToValue(e);

	switch(e->getTypeKind())
	{
		case ty_int:
			return new ConversionExpr(e, co_extension, ty_float);
		case ty_float:
			return e;
		case ty_call:
			return new ConversionExpr(e, co_float, ty_float);
		case ty_pointer:
			return ConvertToValue(e);

		default:
			throw std::runtime_error("Cannot convert the expression to Float type.\n");
	}
}

Expr* Semantics::ConvertToPointer(Expr* e)
{
	switch(e->getTypeKind())
	{
		case ty_pointer:
			return e;
		case ty_char:
		case ty_int:
		case ty_float:
		case ty_bool:
		case ty_reference:
			return new ConversionExpr(e, co_pointer, ty_pointer);

		default:
			throw std::runtime_error("Cannot convert the expression to pointer type.\n");
	}
}

Expr* Semantics::ConvertToReference(Expr* e)
{
	switch(e->getTypeKind())
	{
		case ty_reference:
			return e;
		case ty_char:
		case ty_int:
		case ty_float:
		case ty_bool:
			return new ConversionExpr(e, co_reference, ty_reference);
		case ty_pointer:
			return ConvertToValue(e);
		default:
			throw std::runtime_error("Cannot convert the expression to reference type.\n");
	}
}

Expr* Semantics::ConvertToValue(Expr* e)
{
	if(e->isCall())
	{
		CallExpr* ce = static_cast<CallExpr*>(e);
		IdExpr* id = static_cast<IdExpr*>(ce->getBase());
		Decl* d = curr_scope->lookupRecursive(id->getSymbol());
		FunctionDecl* fd = static_cast<FunctionDecl*>(d);
		CallType* ct = static_cast<CallType*>(fd->getType());
		return new ConversionExpr(e, co_value, ct->getReturnType()->getKind());
	}else if(e->isIdentifer())
	{
		IdExpr* id = static_cast<IdExpr*>(e);
		Decl* d = curr_scope->lookupRecursive(id->getSymbol());
		VariableDecl* vd = static_cast<VariableDecl*>(d);
		return new ConversionExpr(e, co_value, vd->getTypeKind());
	}
	else
		return e;
	

}

Expr* Semantics::ConvertToType(Expr* e, Type* t)
{
	if(t->isScalar() || e->isCall())
	{
		e = ConvertToValue(e);

		type_kind tk = t->getKind();
		if(e->isIdentifer())
		{
			ReferenceType* rt = static_cast<ReferenceType*>(t);
			tk = rt->getObjectKind();
		}

		switch(tk)
		{
			case ty_bool:
				return ConvertToBool(e);
			case ty_char:
				return ConvertToChar(e);
			case ty_int:
				return ConvertToInt(e);
			case ty_float:
				return ConvertToFloat(e);
			case ty_reference:
				return ConvertToReference(e);

			default:
				throw std::runtime_error("Cannot convert the expression to that type.\n");
		}
	}else if(t->isReference())
	{
		return ConvertToReference(e);
	}else if(t->isPointer())
		return ConvertToPointer(e);
	else
		std::runtime_error("Unreconized type conversion");
}
