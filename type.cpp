#include "type.hpp"

Type::Type(type_kind k)
{
	tk = k;
	return;
}

// Checks for the kind of type
bool Type::isBool()
{
	return tk == ty_bool;
}

bool Type::isChar()
{
	return tk == ty_char;	
}

bool Type::isInt()
{
	return tk == ty_int;
}

bool Type::isFloat()
{
	return tk == ty_float;
}

bool Type::isPointer()
{
	return tk == ty_pointer;
}

bool Type::isReference()
{
	return tk == ty_reference;
}

bool Type::isCall()
{
	return tk == ty_call;
}

bool Type::isScalar()
{
	switch(tk)
	{
		case ty_bool:
		case ty_char:
		case ty_int:
		case ty_float:
		// case ty_pointer:
		case ty_call:
			return true;

		default:
			return false;
	}
}

bool Type::isVoid()
{
	return tk == ty_void;
}

bool Type::isTyped()
{
	return tk == ty_reference || tk == ty_pointer;
}