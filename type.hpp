#pragma once
#include <vector>

class Type;
using TypeList = std::vector<Type*>;

// Helper enum for Type 
enum type_kind
{
	ty_bool,
	ty_char,
	ty_int,
	ty_float,
	ty_pointer,
	ty_reference,
	ty_call,
	ty_void,
};


// Type is a data structure for holding type information including check functions
class Type
{
private:
	type_kind tk;

public:

	Type();
	Type(type_kind k);
	virtual ~Type() {} //for dynamic_cast

	type_kind getKind() const {return tk;}
	void setKind(type_kind k) {tk = k;}

	// Checks
	bool isBool();
	bool isChar();
	bool isInt();
	bool isFloat();
	bool isPointer();
	bool isReference();
	bool isCall();
	bool isScalar();
	bool isVoid();
	bool isTyped();
};


struct BoolType : Type
{
	BoolType() : Type(ty_bool) {}
};

struct CharType : Type
{
	CharType() : Type(ty_char) {}
};

struct IntType : Type
{
	IntType() : Type(ty_int) {}
};

struct FloatType : Type
{
	FloatType() : Type(ty_float) {}
};


struct VoidType : Type
{
	VoidType() : Type(ty_void) {}
};

struct CallType : Type
{
	Type* type_return;
	TypeList type_list;

	CallType(TypeList tl, Type* t) : Type(ty_call) 
	{
		type_list = tl;
		type_return = t;
	}

	Type* getReturnType() const {return type_return;}
	TypeList getTypeList() const {return type_list;}
};

struct TypedType : Type
{
	TypedType(Type* t,type_kind tk) : Type(tk)
	{
		object = t;
	}

	Type* object;

	Type* getObject() const {return object;}
	type_kind getObjectKind() const {return object->getKind();}
	Type* getBaseType()
	{
		TypedType* curr = static_cast<TypedType*>(this);
		while(curr->object)
		{
			if(!curr->object->isTyped())
				return curr->object;
			curr = static_cast<TypedType*>(curr->object);
		}
		return curr;
	}
};

struct PointerType : TypedType
{
	PointerType(Type* t) : TypedType(t, ty_pointer) {}
};

struct ReferenceType : TypedType
{
	ReferenceType(Type* t) : TypedType(t, ty_reference) {}
};