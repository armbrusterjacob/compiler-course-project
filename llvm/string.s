	.text
	.file	"string.ll"
	.globl	string_len
	.align	16, 0x90
	.type	string_len,@function
string_len:                             # @string_len
	.cfi_startproc
# BB#0:
	movq	%rdi, -16(%rsp)
	.align	16, 0x90
.LBB0_1:                                # %While
                                        # =>This Inner Loop Header: Depth=1
	movq	-16(%rsp), %rax
	movb	(%rax), %cl
	incq	%rax
	movq	%rax, -16(%rsp)
	incl	-4(%rsp)
	cmpb	$0, %cl
	jne	.LBB0_1
# BB#2:                                 # %Exit
	movl	-4(%rsp), %eax
	decl	%eax
	retq
.Lfunc_end0:
	.size	string_len, .Lfunc_end0-string_len
	.cfi_endproc

	.globl	string_cmp
	.align	16, 0x90
	.type	string_cmp,@function
string_cmp:                             # @string_cmp
	.cfi_startproc
# BB#0:
	movq	%rdi, -8(%rsp)
	movq	%rsi, -16(%rsp)
	jmp	.LBB1_1
	.align	16, 0x90
.LBB1_3:                                # %Incrememnt
                                        #   in Loop: Header=BB1_1 Depth=1
	incq	%rcx
	movq	%rcx, -8(%rsp)
	incq	%rax
	movq	%rax, -16(%rsp)
.LBB1_1:                                # %While
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rsp), %rcx
	movb	(%rcx), %sil
	movq	-16(%rsp), %rax
	movb	(%rax), %r8b
	testb	%sil, %sil
	setne	%dil
	testb	%r8b, %r8b
	setne	%dl
	andb	%dil, %dl
	movzbl	%dl, %edx
	cmpl	$1, %edx
	jne	.LBB1_4
# BB#2:                                 # %While
                                        #   in Loop: Header=BB1_1 Depth=1
	movzbl	%r8b, %edx
	movzbl	%sil, %edi
	cmpl	%edx, %edi
	je	.LBB1_3
.LBB1_4:                                # %GreaterThan
	movzbl	%r8b, %eax
	movzbl	%sil, %ecx
	cmpl	%eax, %ecx
	jbe	.LBB1_5
# BB#7:                                 # %ReturnLHS
	movl	$1, %eax
	retq
.LBB1_5:                                # %LessThan
	jae	.LBB1_6
# BB#8:                                 # %ReturnRHS
	movl	$-1, %eax
	retq
.LBB1_6:                                # %ReturnEqual
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	string_cmp, .Lfunc_end1-string_cmp
	.cfi_endproc

	.globl	string_chr
	.align	16, 0x90
	.type	string_chr,@function
string_chr:                             # @string_chr
	.cfi_startproc
# BB#0:
	movq	%rdi, -8(%rsp)
	movq	$0, -16(%rsp)
	movzbl	%sil, %eax
	jmp	.LBB2_1
	.align	16, 0x90
.LBB2_4:                                # %Incrememnt
                                        #   in Loop: Header=BB2_1 Depth=1
	incq	%rcx
	movq	%rcx, -8(%rsp)
.LBB2_1:                                # %While
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rsp), %rcx
	movzbl	(%rcx), %edx
	cmpl	%edx, %eax
	jne	.LBB2_3
# BB#2:                                 # %NewLast
                                        #   in Loop: Header=BB2_1 Depth=1
	movq	%rcx, -16(%rsp)
.LBB2_3:                                # %CheckEnd
                                        #   in Loop: Header=BB2_1 Depth=1
	testb	%dl, %dl
	jne	.LBB2_4
# BB#5:                                 # %ReturnLast
	movq	-16(%rsp), %rax
	retq
.Lfunc_end2:
	.size	string_chr, .Lfunc_end2-string_chr
	.cfi_endproc

	.globl	string_cpy
	.align	16, 0x90
	.type	string_cpy,@function
string_cpy:                             # @string_cpy
	.cfi_startproc
# BB#0:
	movq	%rsi, -8(%rsp)
	movq	%rdi, -16(%rsp)
	movl	$0, -20(%rsp)
	.align	16, 0x90
.LBB3_1:                                # %While
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rsp), %rcx
	movq	-16(%rsp), %rsi
	movb	(%rcx), %r9b
	movb	%r9b, (%rsi)
	movl	-20(%rsp), %r8d
	leal	1(%r8), %eax
	movl	%eax, -20(%rsp)
	incq	%rcx
	movq	%rcx, -8(%rsp)
	incq	%rsi
	movq	%rsi, -16(%rsp)
	cmpl	%edx, %r8d
	je	.LBB3_3
# BB#2:                                 # %While
                                        #   in Loop: Header=BB3_1 Depth=1
	testb	%r9b, %r9b
	jne	.LBB3_1
.LBB3_3:                                # %Return
	movq	%rdi, %rax
	retq
.Lfunc_end3:
	.size	string_cpy, .Lfunc_end3-string_cpy
	.cfi_endproc

	.globl	string_cat
	.align	16, 0x90
	.type	string_cat,@function
string_cat:                             # @string_cat
	.cfi_startproc
# BB#0:
	movq	%rsi, -8(%rsp)
	movq	%rdi, -16(%rsp)
	.align	16, 0x90
.LBB4_1:                                # %FindEnd
                                        # =>This Inner Loop Header: Depth=1
	movq	-16(%rsp), %rax
	leaq	1(%rax), %rcx
	cmpb	$0, (%rax)
	movq	%rcx, -16(%rsp)
	jne	.LBB4_1
# BB#2:                                 # %FoundEnd
	movq	%rax, -16(%rsp)
	.align	16, 0x90
.LBB4_3:                                # %While
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rsp), %rax
	movq	-16(%rsp), %rcx
	movb	(%rax), %dl
	movb	%dl, (%rcx)
	incq	%rax
	incq	%rcx
	testb	%dl, %dl
	movq	%rax, -8(%rsp)
	movq	%rcx, -16(%rsp)
	jne	.LBB4_3
# BB#4:                                 # %Return
	movq	%rdi, %rax
	retq
.Lfunc_end4:
	.size	string_cat, .Lfunc_end4-string_cat
	.cfi_endproc


	.section	".note.GNU-stack","",@progbits
