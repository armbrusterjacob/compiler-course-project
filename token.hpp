#pragma once
#include <string>
#include "location.hpp"
#include "symbolTable.hpp"

enum tokenName
{
	//Punctuators
    tok_left_brace,
    tok_right_brace,
    tok_left_paren,
    tok_right_paren,
    tok_left_bracket,
    tok_right_bracket,
    tok_comma,
    tok_semicolon,
    tok_colon,

    //Opertators
    //Relational
    tok_equals,
    tok_not_equals,
    tok_less_than,
    tok_greater_than,
    tok_less_than_equals,
    tok_greater_than_equals,
    
    //Arithmetic 
    tok_plus,
    tok_minus,
    tok_multiply,
    tok_divide,
    tok_mod,

    //Bitwise
    tok_shift_left,
    tok_shift_right,
    tok_bit_and,
    tok_bit_or,
    tok_bit_not,
    tok_bit_xor,

    //Logical
    tok_log_and,
    tok_log_or,
    tok_log_not,
    //tilde listed, is also log_not

    //extra operators
    tok_conditional,
    tok_assignment,
    tok_identifier,
    tok_arrow,

    //Keywords
    //Logical operators and various literals are also keywords
    tok_key_and,
    tok_key_as,
    tok_key_bool,
    tok_key_break,
    tok_key_char,
    tok_key_const,
    tok_key_continue,
    tok_key_def,
    tok_key_else,
    tok_key_false,
    tok_key_float,
    tok_key_if,
    tok_key_int,
    tok_key_let,
    tok_key_not,
    tok_key_or,
    tok_key_return,
    tok_key_true,
    tok_key_var,
    tok_key_void,
    tok_key_volatile,
    tok_key_while,

    //Literals
    tok_bin_int,
    tok_dec_int,
    tok_hex_int,
    tok_float,
    tok_bool,
    tok_char,
    tok_string,

    //Type Specifiers
    tok_type_bool,
    tok_type_int,
    tok_type_char,
    tok_type_float,

    //Special Tokens
    tok_eof,
};

enum tokenType
{
    type_punctuator = tok_left_brace,
    type_op_rel = tok_equals,
    type_op_ari = tok_plus,
    type_op_bit = tok_bit_and,
    type_op_log = tok_log_and,
    type_op_ect = tok_conditional,
    type_key = tok_key_if,
    type_lit = tok_bin_int,
    type_spe = tok_type_bool,
};

union tokenAtr
{
    char ch;
    int in;
    float fl;
    bool bo;
    symbol sy;
};

/** Token
    Tokens are the names assigned to sequences of characters.
    Token objects are assigned locations in and optionally
    an attribute if needed for a token.
*/
class Token
{
private:
    tokenName name;
    tokenType type;
    tokenAtr atribute;
    Location *loc;
public:
    Token(const tokenName& token, const Location);
    Token(const tokenName& token, const char& atr, const Location);
    Token(const tokenName& token, const int& atr, const Location);
    Token(const tokenName& token, const float& atr, const Location);
    Token(const tokenName& token, const bool& atr, const Location);
    Token(const tokenName& token, const symbol& atr, const Location);
    tokenName getName();
    tokenAtr getAtr();
    void setType(const int& tn);
    int getLine();
    std::string toString();
    std::string toString(tokenName t);
    Location getLoc();
};