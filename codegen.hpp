#pragma once

class Decl;

/// Generate code for the given declaration.
void generate(const Decl* d);
