#include "token.hpp"

/**
	Constructor
*/

Token::Token(const tokenName & token, const Location l)
{ 
	setType(name);
	loc = new Location(l);
	name = token;
	return;
}

/**
	Constructor with attribute
*/
Token::Token(const tokenName& token, const char& atr, const Location l)
{	
	setType(name);
	loc = new Location(l);
	name = token;
	atribute.ch = atr;
	return;
}

/**
	Constructor with attribute
*/
Token::Token(const tokenName& token, const int& atr, const Location l)
{
	setType(name);
	loc = new Location(l);
	name = token;
	atribute.in = atr;
	return;
}

/**
	Constructor with attribute
*/
Token::Token(const tokenName& token, const float& atr, const Location l)
{
	setType(name);
	loc = new Location(l);
	name = token;
	atribute.fl = atr;
	return;
}

/**
	Constructor with attribute
*/
Token::Token(const tokenName& token, const bool& atr, const Location l)
{
	setType(name);
	loc = new Location(l);
	name = token;
	atribute.bo = atr;
	return;
}

/**
	Constructor with attribute
*/
Token::Token(const tokenName& token, const symbol& atr, const Location l)
{
	setType(name);
	loc = new Location(l);
	name = token;
	atribute.sy = atr;
	return;
}

/**
	getName()
		Returns the token name.
*/
tokenName Token::getName()
{
	return name;
}


/**
	getAtr()
		Return the union attribute value.
*/

tokenAtr Token::getAtr()
{
	return atribute;
}

/**
	getLine()
		Returns the current line from token's location.
*/
int Token::getLine()
{
	return loc->getLine();
}

/**
	setType()
		Sets type by using a provided token name as input.
*/
void Token::setType(const int& tn)
{
	if(tn > type_spe)
		type = type_spe;
	else if(tn > type_lit)
		type = type_lit;
	else if(tn > type_key)
		type = type_key;
	else if(tn > type_op_ect)
		type = type_op_ect;
	else if(tn > type_op_log)
		type = type_op_log;
	else if(tn > type_op_bit)
		type = type_op_bit;
	else if(tn > type_op_ari)
		type = type_op_ari;
	else if(tn > type_op_rel)
		type = type_op_rel;
	else
		type = type_punctuator;

	return;
}

/**
	toString()
		Constructs a string of a Token which will always iclude the both the token's
		type and the token's name. If applicable it will also include the attribute.
*/
std::string Token::toString()
{
	std::string temp = "<";

	switch(type)
	{
		case type_punctuator: temp += "punctautor"; break;
		case type_op_rel: temp += "relational operator"; break;
		case type_op_ari: temp += "arithmetic operator"; break;
		case type_op_bit: temp += "bitwise operator"; break;
		case type_op_log: temp += "logical operator"; break;
		case type_op_ect: temp += "operator"; break;
		case type_key: temp += "keyword"; break;
		case type_lit: temp += "literal"; break;
		case type_spe: temp += "type specifier"; break;
	}

	temp += ":";

	switch(name)
	{
		case tok_left_brace: temp += "left brace"; break;
		case tok_right_brace: temp += "right brace"; break;
		case tok_left_paren: temp += "left paren"; break;
		case tok_right_paren: temp += "right paren"; break;
		case tok_left_bracket: temp += "left bracket"; break;
		case tok_right_bracket: temp += "right bracket"; break;
		case tok_comma: temp += "comma"; break;
		case tok_semicolon: temp += "semiclon"; break;
		case tok_colon: temp += "colon"; break;
		case tok_equals: temp += "equals"; break;
		case tok_not_equals: temp += "not equals"; break;
		case tok_less_than: temp += "less than"; break;
		case tok_greater_than: temp += "greater than"; break;
		case tok_less_than_equals: temp += "less than equals"; break;
		case tok_greater_than_equals: temp += "greater than equals"; break;
		case tok_plus: temp += "plus"; break;
		case tok_minus: temp += "minus"; break;
		case tok_multiply: temp += "multiply"; break;
		case tok_divide: temp += "divide"; break;
		case tok_mod: temp += "mod"; break;
		case tok_bit_and: temp += "bitwise and"; break;
		case tok_bit_or: temp += "bitwise or"; break;
		case tok_bit_not: temp += "bitwise not"; break;
		case tok_bit_xor: temp += "bitwise xor"; break;
		case tok_log_and: temp += "logical and"; break;
		case tok_log_or: temp += "logical or"; break;
		case tok_log_not: temp += "logical not"; break;
		case tok_conditional: temp += "conditional"; break;
		case tok_assignment: temp += "assignemnt"; break;
		case tok_identifier: temp += "identifier"; break;
		case tok_dec_int: temp += "dec int"; break;
		case tok_hex_int: temp += "hex int"; break;
		case tok_bin_int: temp += "bin int"; break;
		case tok_float: temp += "float"; break;
		case tok_bool: temp += "boolean"; break;
		case tok_char: temp += "character"; break;
		case tok_string: temp += "string"; break;
		case tok_type_int: temp += "int specifier"; break;
		case tok_type_bool: temp += "bool specifier"; break;
		case tok_type_char: temp += "char specifier"; break;
		case tok_type_float: temp += "float specifier"; break;
		case tok_key_and: temp += "and"; break;
		case tok_key_as: temp += "as"; break;
		case tok_key_bool: temp += "bool"; break;
		case tok_key_break: temp += "break"; break;
		case tok_key_char: temp += "char"; break;
		case tok_key_const: temp += "const"; break;
		case tok_key_continue: temp += "continue"; break;
		case tok_key_def: temp += "def"; break;
		case tok_key_else: temp += "else"; break;
		case tok_key_false: temp += "false"; break;
		case tok_key_float: temp += "float"; break;
		case tok_key_if: temp += "if"; break;
		case tok_key_int: temp += "int"; break;
		case tok_key_let: temp += "let"; break;
		case tok_key_not: temp += "not"; break;
		case tok_key_or: temp += "or"; break;
		case tok_key_return: temp += "return"; break;
		case tok_key_true: temp += "true"; break;
		case tok_key_var: temp += "var"; break;
		case tok_key_void: temp += "void"; break;
		case tok_key_volatile: temp += "volatile"; break;
		case tok_key_while: temp += "while"; break;
		case tok_eof: temp += "eof"; break;

		default: return "unknown name"; break;
	}

	switch(name)
	{
		case tok_dec_int: 
			temp += ":[" + std::to_string(atribute.in) + "]"; break;
		case tok_hex_int: 
			temp += ":[" + std::to_string(atribute.in) + "]"; break;
		case tok_bin_int: 
			temp += ":[" + std::to_string(atribute.in) + "]"; break;
		case tok_float: 
			temp += ":[" + std::to_string(atribute.fl) + "]"; break;
		case tok_bool: 
			temp += ":[" + std::to_string(atribute.in) + "]"; break;
		case tok_char: 
			temp += ":[" + std::to_string(atribute.ch) + "]"; break;
		case tok_identifier: 
			temp += ":[" + *atribute.sy + "]"; break;
		//case tok_string: return "string"; break;
	}

	temp += ">";

	return temp;
}

std::string Token::toString(tokenName t)
{
	std::string temp = "<";

	switch(t)
	{
		case tok_left_brace: temp += "left brace"; break;
		case tok_right_brace: temp += "right brace"; break;
		case tok_left_paren: temp += "left paren"; break;
		case tok_right_paren: temp += "right paren"; break;
		case tok_left_bracket: temp += "left bracket"; break;
		case tok_right_bracket: temp += "right bracket"; break;
		case tok_comma: temp += "comma"; break;
		case tok_semicolon: temp += "semiclon"; break;
		case tok_colon: temp += "colon"; break;
		case tok_equals: temp += "equals"; break;
		case tok_not_equals: temp += "not equals"; break;
		case tok_less_than: temp += "less than"; break;
		case tok_greater_than: temp += "greater than"; break;
		case tok_less_than_equals: temp += "less than equals"; break;
		case tok_greater_than_equals: temp += "greater than equals"; break;
		case tok_plus: temp += "plus"; break;
		case tok_minus: temp += "minus"; break;
		case tok_multiply: temp += "multiply"; break;
		case tok_divide: temp += "divide"; break;
		case tok_mod: temp += "mod"; break;
		case tok_bit_and: temp += "bitwise and"; break;
		case tok_bit_or: temp += "bitwise or"; break;
		case tok_bit_not: temp += "bitwise not"; break;
		case tok_bit_xor: temp += "bitwise xor"; break;
		case tok_log_and: temp += "logical and"; break;
		case tok_log_or: temp += "logical or"; break;
		case tok_log_not: temp += "logical not"; break;
		case tok_conditional: temp += "conditional"; break;
		case tok_assignment: temp += "assignemnt"; break;
		case tok_identifier: temp += "identifier"; break;
		case tok_dec_int: temp += "dec int"; break;
		case tok_hex_int: temp += "hex int"; break;
		case tok_bin_int: temp += "bin int"; break;
		case tok_float: temp += "float"; break;
		case tok_bool: temp += "boolean"; break;
		case tok_char: temp += "character"; break;
		case tok_string: temp += "string"; break;
		case tok_type_int: temp += "int specifier"; break;
		case tok_type_bool: temp += "bool specifier"; break;
		case tok_type_char: temp += "char specifier"; break;
		case tok_type_float: temp += "float specifier"; break;
		case tok_key_and: temp += "and"; break;
		case tok_key_as: temp += "as"; break;
		case tok_key_bool: temp += "bool"; break;
		case tok_key_break: temp += "break"; break;
		case tok_key_char: temp += "char"; break;
		case tok_key_const: temp += "const"; break;
		case tok_key_continue: temp += "continue"; break;
		case tok_key_def: temp += "def"; break;
		case tok_key_else: temp += "else"; break;
		case tok_key_false: temp += "false"; break;
		case tok_key_float: temp += "float"; break;
		case tok_key_if: temp += "if"; break;
		case tok_key_int: temp += "int"; break;
		case tok_key_let: temp += "let"; break;
		case tok_key_not: temp += "not"; break;
		case tok_key_or: temp += "or"; break;
		case tok_key_return: temp += "return"; break;
		case tok_key_true: temp += "true"; break;
		case tok_key_var: temp += "var"; break;
		case tok_key_void: temp += "void"; break;
		case tok_key_volatile: temp += "volatile"; break;
		case tok_key_while: temp += "while"; break;
		case tok_eof: temp += "eof"; break;

		default: return "unknown name"; break;
	}

	temp += ">";

	return temp;
}

Location Token::getLoc()
{
	Location l = Location(*loc);
	return l;
}