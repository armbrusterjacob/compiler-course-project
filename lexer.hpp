#pragma once

#include <string>
#include <vector>
#include <unordered_map>
#include "token.hpp" 
#include "symbolTable.hpp"
#include "location.hpp"

/** Lexer
	Lexer takes a string as input and then lexes the input into tokens.
	The Lexer works by iterating through the file  in lex() and matching 
	characters form recgonizable sequences.
*/	
class Lexer
{
private:
	std::string file;
	std::string::iterator current;
	std::string::iterator wordStart;
	std::string::iterator eofIt;
	unsigned int wordLength;
	std::vector<Token> tokenVector;
	std::unordered_map<std::string, tokenName> reserved;
	SymbolTable *symT;
	Location *loc;
public:
	Lexer(const std::string& ifs);
	~Lexer()
	{
		delete symT;
		delete loc;
		return;
	} 

	// Lexer(const std::string& fileStr, SymbolTable& sym);

	void lexFile();

	Token lex();
	Token lexGeneral(const int& length, tokenName name);
	Token lexWord();
	Token lexNumber();
	Token lexHex();
	Token lexBin();
	Token lexFloat();
	Token lexEof();

	SymbolTable* getSymbolTable() {return symT;}

	void accept();
	void accept(const int& num);
	void ignore();
	void ignore(const int& num);
	bool eof();
	char peak();
	void resetWord();
	void newLine();

	void printTokens();
};