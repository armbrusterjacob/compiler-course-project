#include "location.hpp"

/**
	Constructor
*/
Location::Location(std::string& str)
{
	column = 0;
	line = 0;
	file = &str;
}

/**
	Copy Constructor
*/
Location::Location(const Location& l)
{
	column = l.column;
	line = l.line;
	file = l.file;
}

/**
	getColumn()
		Returns the location's current column.
*/
int Location::getColumn()
{
	return column;
}

/**
	Returns the location's current line.
*/
int Location::getLine()
{
	return line;
}

/**
	getFile()
		Returns a pointer to the current file string.
*/
std::string* Location::getFile()
{
	return file;
}
 
/**
	newLine()
		Increments the line, advnacing in the file and resets column to zero.
*/
void Location::newLine()
{
	line++;
	column = 0;
	return;
}

/**
	advanceColumn()
		Advances column k times.
*/
void Location::advanceColumn(const int& k)
{
	column += k;
	return;
}

/**
	toString()
		Returns both the current line and column.
*/
std::string Location::toString()
{
	return "[" + std::to_string(line + 1) + 
		   ":" + std::to_string(column) +"]";
}

