#pragma once

#include <iostream>
#include <vector>
#include <stdexcept>
#include "type.hpp"
#include "declaration.hpp"
#include "symbolTable.hpp"

class Decl;
class Expr;
using ExprList = std::vector<Expr*>;

// Helper enum for Expr
enum expr_kind
{
	ex_arithmic,
	ex_int,
	ex_float,
	ex_char,
	ex_bool,
	ex_identifer,
	ex_binary,
	ex_unary,
	ex_conditional,
	ex_cast,
	ex_call,
	ex_index,
	ex_conversion,
};

// Base expression class
class Expr
{
protected:
	expr_kind kind;
	Type* type;

public:

	Expr(expr_kind ex)
	{
		kind = ex;
	}

	Expr(expr_kind ex, type_kind t)
	{
		kind = ex;
		type = new Type(t);
	}

	// virtual ~Expr();

	expr_kind getKind() const {return kind;}
	void setKind(expr_kind t) {kind = t;}

	Type* getType() const {return type;}
	void setType(Type* t) {type = t;}

	type_kind getTypeKind() const {return type->getKind();}

	bool isArithmic();
	bool isInt();
	bool isFloat();
	bool isChar();
	bool isBool();
	bool isIdentifer();
	bool isBinary();
	bool isUnary();
	bool isConditional();
	bool isCast();
	bool isIndex();
	bool isConversion();
	bool isCall();
};


// Char literal for primary expression
struct CharExpr : Expr
{
	char val;

	CharExpr(char v) : Expr(ex_char, ty_char)
	{
		val = v;
	}

	char value() const {return val;}

};

// Int literal for primary expression
struct IntExpr : Expr
{
	int val;

	IntExpr(int v) : Expr(ex_int, ty_int)
	{
		val = v;
	}

	int value() const {return val;}
};

// Float literal for primary expression
struct FloatExpr : Expr
{
	float val;

	FloatExpr(float v) : Expr(ex_float, ty_float)
	{
		val = v;
	}

	float value() const {return val;}
};

// Bool literal for primary expression
struct BoolExpr : Expr
{
	bool val;

	BoolExpr(bool v) : Expr(ex_bool, ty_bool)
	{
		val = v;
	}

	bool value() const {return val;}
};



// Identifiers for primary expression
struct  IdExpr : Expr
{
	Decl* decl;
	symbol sym;
	IdExpr(Decl* d, Type* t, symbol s) : Expr(ex_identifer)
	{
		decl = d;
		type = t;
		sym = s;
	}
	symbol getSymbol() const {return sym;}
	Decl* getDecl() const {return decl;}
};

//currently no string literals or arrays

struct CastExpr : Expr
{
	Type* cast;
	Expr* e1;

	CastExpr(Expr* e, Type* t) : Expr(ex_cast)
	{
		cast = t;
		e1 = e;
	}
};


struct ConditionalExpr : Expr
{
	Expr* condition;
	Expr* on_true;
	Expr* on_false;

	ConditionalExpr(Expr* c, Expr* t, Expr* f) : Expr(ex_conditional, ty_bool)
	{
		condition = c;
		on_true = t;
		on_false = f;
	}

	Expr* getCondExpr() const {return condition;}
	Expr* getTrueExpr() const {return on_true;}
	Expr* getFalseExpr() const {return on_false;}
};	

// Helper for unaryExpr
enum unary_operator_type
{
	un_add,
	un_subtract,
	un_bit_not,
	un_log_not,
	un_pointer,
	un_reference,
	un_dereference,
};

struct UnaryExpr : Expr
{
	unary_operator_type un;
	Expr* e1;

	UnaryExpr(unary_operator_type u, Expr* e) : Expr(ex_unary, e->getTypeKind())
	{
		e1 = e;
		un = u;
	}

	unary_operator_type getUnaryOp() const {return un;}
	Expr* getExpr() const {return e1;}
};

// Arithmetic and binary operators
enum bianry_operator_kind
{
	bn_multiply,
	bn_divide,
	bn_mod,
	bn_add,
	bn_subtract,
	bn_bit_shift_left,
	bn_bit_shift_right,
	bn_bit_relational,
	bn_bit_and,
	bn_bit_or,
	bn_bit_xor,
	bn_log_and,
	bn_log_or,
	bn_assignment,
	bn_equality,
	bn_inequality,
	bn_less_than,
	bn_less_than_equals,
	bn_greater_than,
	bn_greater_than_equals,
};

struct BinaryExpr : Expr
{
	Expr* e1;
	Expr* e2;
	bianry_operator_kind op;

	BinaryExpr(bianry_operator_kind o, Expr* l, Expr* r, Type* t) : Expr(ex_binary)
	{
		op = o;
		e1 = l;
		e2 = r;
		type = t;
	}
	BinaryExpr(bianry_operator_kind o, Expr* l, Expr* r, type_kind tk) : Expr(ex_binary, tk)
	{
		op = o;
		e1 = l;
		e2 = r;	
	}

	bianry_operator_kind getBinaryOp() const {return op;}
	Expr* getLeftExpr() const {return e1;}
	Expr* getRightExpr() const {return e2;}
};

// Funciton version of postfix Expression
struct CallExpr : Expr
{
	Expr* base;
	ExprList  expr_list;

	CallExpr(Expr* e, ExprList& list, Type* ct) : Expr(ex_call, ty_call)
	{
		base = e;
		expr_list = list;
		type = ct;
	}

	Expr* getBase() const {return base;}
	ExprList getList() const {return expr_list;}
};

// Index version of postfix Expression
struct IndexExpr : Expr
{
	//Not implemented for this compiler

	// Expr* base;
	// std::vector<Expr*> expr_list;

	// PostfixIndexExpr(Expr* e, std::vector<Expr*>& list) 
	// : Expr(ex_index, ty_index)
	// {
	// 	base = e;
	// 	expr_list = list;
	// }
};

enum conv_kind
{
	co_bool,
	co_char,
	co_int,
	co_float,
	co_pointer,
	co_reference,
	co_call,
	co_trunaction,
	co_extension,
	co_value,
};

struct ConversionExpr : Expr
{
	Expr* base;
	conv_kind conv;

	ConversionExpr(Expr* e, conv_kind ck, type_kind tk) : Expr(ex_conversion, tk)
	{
		base = e;
		conv = ck;
	}

	Expr* getBase() const {return base;}
	conv_kind getConversion() const {return conv;}
};