#pragma once

#include <string>
#include "expression.hpp"
#include "type.hpp"
#include "statement.hpp"
#include "declaration.hpp"
#include "token.hpp"
#include "scope.hpp"

/*
	Semantics dictate the flow of the program using statements, declarations, 
	types and expressions
*/

class Semantics
{
private: 
	Scope* global_scope;
	Scope* curr_scope;
public:

	Semantics()
	{
		global_scope = nullptr;
		curr_scope = nullptr;
	}	

	// -- Expressions --
	Expr* on_cast_expression(Expr* e, Type* t);
	Expr* on_multiplicative_expression(Expr* e1, Expr* e2, Token tok);
	Expr* on_additive_expression(Expr* e1, Expr* e2, Token tok);
	Expr* on_shift_expression(Expr* e1, Expr* e2, Token tok);
	Expr* on_relational_expression(Expr* e1, Expr* e2, Token tok);
	Expr* on_equality_expression(Expr* e1, Expr* e2, Token tok);
	Expr* on_bitwise_expression(Expr* e1, Expr* e2, Token tok);
	Expr* on_logical_expression(Expr* e1, Expr* e2, Token tok);
	Expr* on_conditional_expression(Expr* e1, Expr* e2, Expr* e3);
	Expr* on_assignment_expression(Expr* e1, Expr* e2);
	Expr* on_int_literal(Token tok);
	Expr* on_float_literal(Token tok);
	Expr* on_char_literal(Token tok);
	Expr* on_bool_literal(Token tok);
	Expr* on_identifier(Token tok);
	Expr* on_function_call(Expr* e, ExprList& args);
	Expr* on_index(Expr* e, ExprList& args);
	Expr* on_unary(Token tok, Expr* e);

	// -- Types --
	Type* on_basic_type(Token tok);
	Type* on_function_type(TypeList& tl, Type* t);
	Type* on_pointer_type(Type* t);

	// -- Statements --
	Stmt* on_block_statement(StmtList& sl);
	Stmt* on_if_statement(Expr* e, Stmt* s1, Stmt* s2);
	Stmt* on_if_statement(Expr* e, Stmt* s);
	Stmt* on_while_statement(Expr* e, Stmt* s);
	Stmt* on_continue_statement();
	Stmt* on_break_statement();
	Stmt* on_return_statement();
	Stmt* on_return_statement(Expr* e);
	Stmt* on_declaration_statement(Decl* d);
	Stmt* on_expression_statement(Expr* e);

	// -- Declarations --
	Decl* on_program_declaration(DeclList& dl);
	Decl* on_variable_declaration(Token tok, Expr* e, Type* t);
	Decl* on_variable_declaration(Token tok, Type* t);
	Decl* on_constant_declaration(Token tok, Expr* e, Type* t);
	Decl* on_value_declaration(Token tok, Expr* e, Type* t);
	Decl* on_function_declaration(Token tok, DeclList& params, Stmt* s, Type* t);
	Decl* on_parameter_declaration(Token tok, Type* t);

	// -- Scope Functions --
	void declareToScope(Token tok, Decl* d);
	void enterGlobalScope();
	void enterParameterScope();
	void enterBlockScope();
	void exitScope();
	Scope* getCurrentScope();
	Scope* getGlobalScope();

	// -- ect --
	void throwSymbolError(std::string str, symbol s);
	void throwLocationError(std::string str, Token tok);

	// Error Testing
	Expr* requireBool(Expr* e);
	Expr* requireChar(Expr* e);
	Expr* requireInt(Expr* e);
	Expr* requireFloat(Expr* e);
	Expr* requirePointer(Expr* e);
	Expr* requireReference(Expr* e);
	Expr* requireCall(Expr* e);
	Expr* requireArithmetic(Expr* e);
	Type* requireSame(Expr*& e1, Expr*& e2);

	Expr* ConvertToBool(Expr* e);
	Expr* ConvertToChar(Expr* e);
	Expr* ConvertToInt(Expr* e);
	Expr* ConvertToFloat(Expr* e);
	Expr* ConvertToPointer(Expr* e);
	Expr* ConvertToReference(Expr* e);
	Expr* ConvertToValue(Expr* e);
	Expr* ConvertToType(Expr* e, Type* t);
};