#include "declaration.hpp"
#include <iostream>

bool Decl::is_program() const

{	return decl == dl_program;	
}

bool Decl::is_function() const
{
	return decl == dl_function;	
}

bool Decl::is_variable() const
{
	return decl == dl_variable;	
}

bool Decl::is_cosntant() const
{
	return decl == dl_cosntant;	
}

bool Decl::is_value() const
{
	return decl == dl_value;	
}

bool Decl::is_parameter() const
{
	return decl == dl_parameter;	
}

std::string Decl::getName() const
{
	return std::string(*symb);
}