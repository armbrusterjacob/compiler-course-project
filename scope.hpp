#pragma once

#include <iostream>
#include <unordered_map>
#include "symbolTable.hpp"
#include "declaration.hpp"

enum scope_type
{
	sc_global,
	sc_block,
	sc_parameter,
};

class Scope
{
private:
	std::unordered_map<symbol, Decl*> decl_map;
	scope_type st;
	Scope* parent;

public:
	Scope(scope_type s, Scope* p)
	{
		st = s;
		parent = p;
	}
	Scope(scope_type s)
	{
		st = s;
		// parent = nullptr;
	}

	Scope(const Scope& sc)
	{
		decl_map = sc.decl_map;
		st = sc.st;
		parent = sc.parent;
	}

	void addToScope(symbol s, Decl* d);
	void addChildScope(Decl* d, Scope* sc);
	Decl* lookup(symbol s);
	Decl* lookupRecursive(symbol s);
	Scope* getParent() {return parent;}
	std::unordered_map<symbol, Decl*> getDeclMap() {return decl_map;}
	bool isLeaf();
};

struct GlobalScope : Scope
{
	GlobalScope() : Scope(sc_global) {}
};

struct BlockScope : Scope
{
	BlockScope(Scope* p) : Scope(sc_block, p) {}
};

struct ParameterScope : Scope
{
	ParameterScope(Scope* p) : Scope(sc_parameter, p) {}
};
